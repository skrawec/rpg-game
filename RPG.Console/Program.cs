﻿using RPG.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //TODO LIST
            //Heavy Overhaul of Display System (took out all the string outputs inside the code
            //Switched to a Battle and Turn Tracker
            //Turn Tracker has many Target Trackers and Target Tracker has many Object State Trackers
            //Returning this to the UI or in this case the ASCII UI I can display info based on that and remove the strings
            //[✔]  - Remove all Strings Outputs
            //[✔]  - Move TurnTracker into all methods that have to do with combat turns
            //[]  - Debug and refactor
            //[]  - Create Display module for UI
            //
            //Make battlefield (array?, matrix?)
            //[]  - Moving
            //[]  - Distance check for targets
            //
            //New Weapons
            //[]  - projectile weapons
            //[✔]  - Different weapon type weapons (mace for blunt)
            //
            //Morale/Fear system
            //[]  - Losing limbs takes a large toll
            //[]  - Friends dying takes a large toll
            //[]  - Some people have resistances - berserker will have a larger pool
            //[]  - Gain some for killing? Winnin? per turn
            //
            //Incoming Damage type has more effect
            //[]  - Changes destroyed wording
            //[]  - Add wording for regular hit
            //[✔]  - Undestroyable damage?  
            //[✔]  - Some types cannot sever limbs
            //[✔]  - example (Bleeding cannot sever a limb)
            //Attacking
            //[]  - Attacking without skill in weapon causes more strain
            //[]  - Heavy weapons without strength will take a variable amount of strain
            //Waiting
            //[]  - Waiting a round can regain some strain
            //
            //Add test creature that is not humanoid
            //[✔]  - Auto generated enemies
            //[]  - Robot?
            //[✔]  - Wolf
            //[]  - Randomly create creatures
            //
            //Add more actions
            //[✔]  - Manifesting
            //[✔]      - Call Fire test
            //[]  - Skills
            //[]  - Guarding
            //[]  - Parry (Str based on parry, Dex/Agi based chance, 0 in skill high chance to lose balance)            
            //[✔]  - Use Items - Bandages?
            //[✔]  - DamageDescription over top of everything, pass it down
            //[✔]  - DamageDescription Handles multiple attacks/hits
            // Body Strain Mechanic
            //[✔]  - Actions now take BodyStrain cost
            //[✔]  - SubActions will reduce current body strain
            //[✔]  - Body Strain will regen every turn
            //[]  - difference in body and mind strain?
            //[]  - Negative strain effects
            //[✔]  - Negative strain causes fatigue for now (skip turn)
            //
            //Battle Items
            //[✔]  - Bandages (repair bleeding)
            //[]  - Fix Item Code, it's horrible
            //[]  - Drugs
            //[]  - Bullets/Arrows Ammunition
            //
            //Improved Targeting
            //[]  - Misses
            //[]  - Crits
            //[✔]  - Percentage targeting
            //[✔]      - Upper torso should be easier to hit than elbow
            //[]  - Dynamic percentage targeting
            //[]      - Equipping shield should effect ratios
            //[]      - Smaller creature has higher chance of hitting lower body
            //Improved Calculations (Damage, Targeting, Skills)
            //[]  - Balance numbers
            //[]  - Tiered HP system at different levels of volume (Min, Max, Tiers)
            //Improved AI
            //[]  - Once more Actions are in make better desicions
            //[]      - based on stats
            //[]      - based on behaviour
            //
            //Natural Weapon Limbs
            //[✔]  - Added limbs that can be weapons
            //[]  - Variable attacks to one attack
            //[]    - Kicks?
            //[]    - Headbutts?
            //
            //More Limb Effects
            //[✔]  - Poison                                     
            //[✔]  - Spreadable effects
            //[✔]  - Resistable effects?
            //[]    - Burning
            //[]    - Frozen
            //[✔]       - Losing turns?
            //[]    - Healing Effect
            //
            //More Creature Effects
            //[]  - Prone
            //[]  - Unconsious
            //[✔]  - Stunned
            //[]  - Starving
            //[]  - 


            Random random = new Random();

            //ALLIES

            Player stephan = GenerateCreature.Player("Stephan", Gender.Male, 16, 18, 12, 12, 14);
            stephan.EquipWeapon(stephan.GetWeaponMountableLimbs()[0], GenerateCreature.Sword("Steel Sword", 1));
            stephan.EquipWeapon(stephan.GetWeaponMountableLimbs()[1], GenerateCreature.Mace("Steel Mace", 2));
            stephan.EquipArmor(stephan.Limbs[0].AttachedLimbs[0].AttachedLimbs[0], GenerateCreature.Helmet("Helm"));
            stephan.EquipArmor(stephan.Limbs[0].AttachedLimbs[1].AttachedLimbs[0].AttachedLimbs[0].AttachedLimbs[0], GenerateCreature.Helmet("Wrist"));
            stephan.Items.Add(new Bandage(stephan));
            stephan.Items.Add(new Bandage(stephan));
            stephan.Manifestations.Add(new Fire(10, stephan));


            Creature hope = GenerateCreature.Humanoid("Hope", Gender.Female, 9, 15, 18, 17, 14);
            hope.EquipWeapon(hope.GetWeaponMountableLimbs()[0], GenerateCreature.Mace("Steel Mace", 3));
            hope.Manifestations.Add(new Fire(10, hope));


            hope.Manifestations.Add(new HealingTouch(hope, 10));

            Creature chris = GenerateCreature.Humanoid("Chris", Gender.Male, 15, 15, 15, 8, 12);
            chris.EquipWeapon(chris.GetWeaponMountableLimbs()[0], GenerateCreature.Sword("Steel Katana", 3));

            Creature phanat = GenerateCreature.Humanoid("Phanat", Gender.Male, 15, 16, 8, 11, 8);
            phanat.EquipWeapon(phanat.GetWeaponMountableLimbs()[0], GenerateCreature.Sword("Steel Sword", 3));

            Creature ilian = GenerateCreature.Humanoid("Ilian", Gender.Male, 15, 16, 8, 15, 20);
            ilian.EquipWeapon(ilian.GetWeaponMountableLimbs()[0], GenerateCreature.Sword("Steel Sword", 3));

            //ENEMIES

            //Creature matt = GenerateCreature.Humanoid("Matt", Gender.Male, 14, 12, 15, 11, 10);
            //matt.EquipWeapon(matt.GetWeaponMountableLimbs()[0], GenerateCreature.Sword("Steel Spear", 1));
            //matt.EquipArmor(matt.Limbs[0].AttachedLimbs[0].AttachedLimbs[0], GenerateCreature.Helmet("Helm"));
            //matt.EquipArmor(matt.Limbs[0], GenerateCreature.Helmet("Steel Chest"));
            //matt.EquipArmor(matt.Limbs[0].AttachedLimbs[3], GenerateCreature.Helmet("Steel Waistband"));

            //Creature justin = GenerateCreature.Humanoid("Justin", Gender.Male, 15, 12, 16, 9, 12);
            //justin.EquipWeapon(justin.GetWeaponMountableLimbs()[0], GenerateCreature.Sword("Steel Sword", 3));
            //justin.Manifestations.Add(new Fire(10, justin));

            Creature jared = GenerateCreature.Humanoid("Jared", Gender.Male, 14, 11, 10, 10, 14);
            jared.EquipWeapon(jared.GetWeaponMountableLimbs()[0], GenerateCreature.Mace("Steel Mace", 3));
            jared.Manifestations.Add(new Fire(10, jared));

            Creature wolf = GenerateCreature.GenerateWolf("Wolf 1", 12,4,16,4,8,1);
            Creature wolf2 = GenerateCreature.GenerateWolf("Wolf 2", 12, 4, 16, 4, 8, 1);

            List<Creature> enemies = new List<Creature>();
            List<Creature> allies = new List<Creature>();

            //enemies.Add(justin);
            //enemies.Add(chris);
            //enemies.Add(matt);
            //enemies.Add(phanat);
            enemies.Add(wolf);
            enemies.Add(wolf2);

            allies.Add(stephan);
            allies.Add(hope);
            allies.Add(jared);
            allies.Add(ilian);

            Location location = Location.Cave;

            Battle.Start(enemies, allies, location, random);
        }
    } 

}



