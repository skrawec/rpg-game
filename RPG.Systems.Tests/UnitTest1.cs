﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RPG.Systems.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void FindIfEffectIsSkipped()
        {
            Creature player = GenerateCreature.Humanoid("Player1", Gender.Male,10,10,10,10, 25);
            Limb limb = player.Limbs[0].AttachedLimbs[1].AttachedLimbs[0];
            //Limb limb = new Limb("limb", player, 10, 10, false, player.Limbs[0].Material, player.Limbs[0].Shape);
            limb.Effects.Add(new Bleeding(limb, 10));
            //limb.Effects.Add(new Poison("Snake's Venom", Severity.Low, 1, 0));


            


            Random random = new Random();
            TurnTracker turnTracker = new TurnTracker();

            TargetTracker targetTracker = new TargetTracker(player);
            turnTracker.Targets.Add(targetTracker);

            Poison poison = new Poison("Snake's Venom", Severity.Low, 1, 0);
            poison.Apply(targetTracker, limb);

            player.RunOnGoingEffects(turnTracker, random);
            player.RunOnGoingEffects(turnTracker, random);
            player.RunOnGoingEffects(turnTracker, random);
            player.RunOnGoingEffects(turnTracker, random);

            //var turn1Reist = limb.TryResistOnGoingEffects(random);
            //var turn1Ongoing = limb.RunOnGoingEffects(random);
            //var turn1Spread = limb.TrySpreadOnGoingEffects(random);

            //var turn2Reist = limb.TryResistOnGoingEffects(random);
            //var turn2Ongoing = limb.RunOnGoingEffects(random);            
            //var turn2Spread = limb.TrySpreadOnGoingEffects(random);
            //    Weapon sword = Generate.Sword("Sword", 1);

            //    Assert.AreEqual(80, player.TotalCurrentHitPoints);

            //    player.Limbs[1].Damage(new AttackActionTEST() { Weapon = sword });

            //    Assert.AreEqual(80, player.TotalCurrentHitPoints);
        }
       
    }
}
