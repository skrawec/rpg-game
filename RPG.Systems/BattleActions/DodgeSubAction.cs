﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class DodgeSubAction : SubAction
    {
        public DodgeSubAction(string name, Creature user)
        {
            Name = name;
            _user = user;
        }

        private Creature _user;

        #region ISubAction Members

        public override void ExecuteAction(TurnTracker turnTracker, Random random)
        {
            //TODO: Probably delete this, useless   
        } 

        #endregion
    }
}
