﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public interface IAction  
    {
        bool IsUseable();
        List<SubAction> SubActions { get; }
        string Name { get; }
        
    }
}
