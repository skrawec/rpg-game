﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public static class ActionsFactory
    {
        public static List<IAction> GetActionsFor(Creature currentCombatant)
        {
            List<IAction> actions = new List<IAction>();
            actions.Add(new AttackAction(currentCombatant));
            //actions.Add(new DodgeAction(currentCombatant));
            actions.Add(new ItemAction(currentCombatant));
            actions.Add(new ManifestAction(currentCombatant));
            //add more actions

            actions = actions.Where(action => action.IsUseable()).ToList();

            return actions;
        }


    }
}

