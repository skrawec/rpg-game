﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class ManifestSubAction : SubAction
    {
        public ManifestSubAction(IManifestation manifest, Creature user)
        {
            Name = manifest.Name;
            AutoTarget = manifest.AutoTarget;
            LimbTarget = manifest.LimbTarget;
            DefaultTargetsEnemies = manifest.DefaultTargetEnemies;
            _manifest = manifest;
            StrainCost = manifest.StrainCost;
            _user = user;
        }

        private IManifestation _manifest;
        private Creature _user;

        #region SubAction Members

        public override void ExecuteAction(TurnTracker turnTracker, Random random)
        {            
            turnTracker.ActionUsed = this;

            //use body strain by stain cost of sub action
            _user.CurrentBodyStrain -= StrainCost;            

            var targetCreature = _manifest as ITargetCreature;
            if (targetCreature != null)
            {
                targetCreature.Use(turnTracker, Target, random);
            }

            var targetLimb = _manifest as ITargetLimb;
            if (targetLimb != null)
            {
                targetLimb.Use(turnTracker, TargetLimb, random);
            }

            var targetAll = _manifest as ITargetAll;
            if (targetAll != null)
            {
                targetAll.Use(turnTracker, GroupTarget, random);
            }

            var target = _manifest as ITarget;
            if (target != null)
            {
                target.Use(turnTracker, random);
            }
        } 

	    #endregion



    }
}
