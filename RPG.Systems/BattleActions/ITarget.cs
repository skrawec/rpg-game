﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public interface ITarget
    {
        void Use(TurnTracker turnTracker, Random random);
    }
}
