﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public abstract class SubAction
    {
        public string Name { get; set; }
        public int StrainCost { get; set; }
        public bool AutoTarget { get; set; }
        public bool LimbTarget { get; set;  }
        public bool DefaultTargetsEnemies { get; set;  }
        public Creature Target { get; set; }
        public Limb TargetLimb { get; set; }
        public List<Creature> GroupTarget { get; set; }

        public abstract void ExecuteAction(TurnTracker turnTracker, Random random);

        public virtual void FatigueCheck(Creature user, TargetTracker targetTracker)
        {
            if (user.CurrentBodyStrain < 0 &&
                !user.Effects.Any(effect => effect is Fatigue))
            {
                var fatigue = new Fatigue(user);
                fatigue.Apply(targetTracker);

            }
        }
    }
}
