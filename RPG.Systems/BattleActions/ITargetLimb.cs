﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public interface ITargetLimb
    {
        void Use(TurnTracker turnTracker, Limb limb, Random random);
    }
}
