﻿using System;
using System.Text;

namespace RPG.Systems
{
    public interface ITargetCreature
    {
        void Use(TurnTracker turnTracker, Creature creature, Random random);
    }
}
