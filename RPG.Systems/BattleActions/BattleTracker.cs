﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class BattleTracker
    {
        public BattleTracker()
        {
            Turns = new List<TurnTracker>();
        }

        public List<TurnTracker> Turns { get; set; }
    }
}
