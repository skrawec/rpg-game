﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class ManifestAction : IAction
    {
        public ManifestAction(Creature currentCombatant)
        {
            SubActions = new List<SubAction>();
            _user = currentCombatant;
            GetSubActions();
            Name = "Manifest";
        }

        private Creature _user;

        #region IAction Members

        public List<SubAction> SubActions { get; private set; }
        public string Name { get; private set; }

        public bool IsUseable()
        {
            bool useable = false;

            //get head part
            
            var heads = _user.GetLimbsOfType(typeof(Head));

            //If manifestations exist and user is not disabled
            if (_user.Manifestations.Count > 0 && 
                !_user.IsDisabled)
            {
                //if at least one head is not disabled switch to true
                foreach (var head in heads)
                {
                    if (!head.IsDisabled)
                    {
                        useable = true;
                    }
                }
                
            }

            return useable;
        }

        #endregion

        private void GetSubActions()
        {
            foreach (var manifest in _user.Manifestations)
            {
                SubActions.Add(new ManifestSubAction(manifest, _user));
            }
        }
    }
}
