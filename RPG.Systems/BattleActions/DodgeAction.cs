﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class DodgeAction : IAction
    {
        public DodgeAction(Creature currentCombatant)
        {
            SubActions = new List<SubAction>();
            _user = currentCombatant;
            GetSubActions();
        }

        private void GetSubActions()
        {
            SubActions.Add(new DodgeSubAction("Dodge Left", _user));
            SubActions.Add(new DodgeSubAction("Dodge Right", _user));
            if (_user.Agility > 18)
            {
                SubActions.Add(new DodgeSubAction("Dodge Backwards", _user));
            }
            if (_user.Agility > 20)
            {
                SubActions.Add(new DodgeSubAction("Dodge Forwards", _user));
            }
        }

        private Creature _user;

        #region IAction Members

        public List<SubAction> SubActions { get; private set; }
        public string Name { get { return "Dodge"; } }

        public bool IsUseable()
        {
            bool usable = false;

            if (_user.Agility > 15 &&
                !_user.IsDisabled)
            {
                usable = true;
            }

            return usable;
        } 

        #endregion
    }
}
