﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class AttackSubAction : SubAction
    {
        public AttackSubAction(string name, IAttack attack, Creature user)
        {
            Name = name;
            _user = user;
            _attack = attack;
            DefaultTargetsEnemies = true;
        }

        private Creature _user;
        private IAttack _attack;

        #region ISubAction Members

        public override void ExecuteAction(TurnTracker turnTracker, Random random)
        {
            Object targetedObject = Targeting.RandomLimb(Target, random);
            turnTracker.ActionUsed = this;
            targetedObject.Damage(turnTracker, _attack, random);
            
            //use body strain by stain cost of sub action
            _user.CurrentBodyStrain -= StrainCost;
        } 

        #endregion
    }
}
