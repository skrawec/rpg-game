﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class TurnTracker
    {
        public TurnTracker()
        {
            Targets = new List<TargetTracker>();
            DeadCreatures = new List<Creature>();
        }
                          
        public Creature TurnTaker { get; set; }
        public List<TargetTracker> Targets { get; set; }
        public IEnumerable<Object> ObjectsDropped { get { return Targets.SelectMany(x => x.ObjectDropped); } }
        public IEnumerable<Equipment> EquipmentDropped { get { return Targets.SelectMany(x => x.EquipmentDropped); } }
        public SubAction ActionUsed { get; set; }
        public List<Creature> DeadCreatures { get; set; }

        public string TextOutput(Random random)
        {
            StringBuilder sb = new StringBuilder();

            //Action used output
            sb.AppendFormat("{0} used {1}{2}", TurnTaker.Name, ActionUsed.Name, Environment.NewLine);

            //Damage Output
            var groupedTargets = Targets.GroupBy(x => x.Target);

            foreach (var targets in groupedTargets)
            {
                sb.AppendFormat("{0}:{1}", targets.Key.Name, Environment.NewLine);

                var groupedObjects = targets.SelectMany(y => y.ObjectStateTrackers.GroupBy(x => x.TargetObject));

                #region Object Section

                foreach (var group in groupedObjects)
                {
                    var groupedByAttackType = group.GroupBy(x => x.AttackType);

                    string descriptiveWord = "";

                    if (group.Key is Limb)
                    {
                        descriptiveWord = (group.Key as Limb).Side.ToString();
                    }
                    else if (group.Key is Piece)
                    {
                        descriptiveWord = (group.Key as Piece).EquipmentOwner.Name + "'s";
                    }

                    sb.AppendFormat("\t{0} {1}:{2}", descriptiveWord, group.Key.WorldlyName, Environment.NewLine);

                    foreach (var attackTypeGroup in groupedByAttackType)
                    {
                        foreach (var damagableAttacks in attackTypeGroup.Where(x => x.DoesDamage))
                        {
                            int totalDamage = attackTypeGroup.Sum(x => x.Damage);



                            sb.AppendFormat("\t\tTakes {0} {1} damage{2}", totalDamage, attackTypeGroup.Key.ToString(), Environment.NewLine);

                            if (damagableAttacks.IsDamaged)
                            {
                                sb.AppendFormat("\t\tIs damaged{0}", Environment.NewLine);
                            }

                            if (damagableAttacks.IsDestroyed)
                            {
                                sb.AppendFormat("\t\tIs {0}{1}", group.Key.GetDestroyedWord(random), Environment.NewLine);
                            }
                        }

                        foreach (var nonDamagableAttacks in attackTypeGroup.Where(x => !x.DoesDamage))
                        {
                            if (nonDamagableAttacks.EffectsAdded.Count > 0)
                            {
                                foreach (var effect in nonDamagableAttacks.EffectsAdded)
                                {
                                    sb.AppendFormat("\t\tIs now {0}{1}", effect.Name, Environment.NewLine);
                                }
                            }

                            if (nonDamagableAttacks.EffectsRemoved.Count > 0)
                            {
                                foreach (var effect in nonDamagableAttacks.EffectsAdded)
                                {
                                    sb.AppendFormat("\t\t{0} has been removed{1}", effect.Name, Environment.NewLine);
                                }
                            }

                            if (nonDamagableAttacks.Healing > 0)
                            {
                                sb.AppendFormat("\t\tHeals {0} {1} damage{2}", nonDamagableAttacks.Healing, attackTypeGroup.Key.ToString(), Environment.NewLine);
                            }

                            if (nonDamagableAttacks.IsDamaged)
                            {
                                sb.AppendFormat("\t\tIs damaged{0}", Environment.NewLine);
                            }

                            if (nonDamagableAttacks.IsDestroyed)
                            {
                                sb.AppendFormat("\t\tIs {0}{1}", group.Key.GetDestroyedWord(random), Environment.NewLine);
                            }

                        }
                    }
                }
                #endregion

                #region Dropped Section


                if (targets.SelectMany(x => x.EquipmentDropped).Count() > 0 || targets.SelectMany(x => x.ObjectDropped).Count() > 0)
                {
                    sb.AppendFormat("\tDropped:{0}", Environment.NewLine);

                    foreach (var droppedEquipment in targets.SelectMany(x => x.EquipmentDropped))
                    {
                        sb.AppendFormat("\t\t{0} dropped to the ground{1}", droppedEquipment.Name, Environment.NewLine);
                    }

                    foreach (var droppedObject in targets.SelectMany(x => x.ObjectDropped))
                    {
                        sb.AppendFormat("\t\t{0} dropped to the ground{1}", droppedObject.WorldlyName, Environment.NewLine);
                    }
                }
                #endregion

            }

            #region Dead Section

            foreach (var deadCreature in DeadCreatures)
            {
                sb.AppendFormat("{0} had died{1}", deadCreature.Name, Environment.NewLine);
            }

            #endregion

            return sb.ToString();
        }
    }

    public class TargetTracker
    {
        public TargetTracker(ObjectStateTracker limbTracker)
        {
            ObjectStateTrackers = new List<ObjectStateTracker>();
            ObjectStateTrackers.Add(limbTracker);
            EffectsAdded = new List<ICreatureEffect>();
            EffectsRemoved = new List<ICreatureEffect>();
            EquipmentDropped = new List<Equipment>();
            ObjectDropped = new List<Object>();
            ItemsUsedOn = new List<Item>();
        }

        public TargetTracker(Creature target)
        {
            Target = target;
            ObjectStateTrackers = new List<ObjectStateTracker>();
            EffectsAdded = new List<ICreatureEffect>();
            EffectsRemoved = new List<ICreatureEffect>();
            EquipmentDropped = new List<Equipment>();
            ObjectDropped = new List<Object>();
            ItemsUsedOn = new List<Item>();
        }

        public TargetTracker(Creature target, ObjectStateTracker objectStateTracker)
        {
            Target = target;
            ObjectStateTrackers = new List<ObjectStateTracker>();
            ObjectStateTrackers.Add(objectStateTracker);
            EffectsAdded = new List<ICreatureEffect>();
            EffectsRemoved = new List<ICreatureEffect>();
            EquipmentDropped = new List<Equipment>();
            ObjectDropped = new List<Object>();
            ItemsUsedOn = new List<Item>();
         }

        public Creature Target { get; set; }
        public List<ObjectStateTracker> ObjectStateTrackers { get; set; }
        public int ReducedLimbMaxDurability { get; set; }
        public int ReducedArmorMaxDurability { get; set; }
        public List<ICreatureEffect> EffectsAdded { get; set; }
        public List<ICreatureEffect> EffectsExtended { get; set; }
        public List<ICreatureEffect> EffectsRemoved { get; set; }
        public List<Equipment> EquipmentDropped { get; set; }
        public List<Object> ObjectDropped { get; set; }
        public List<Item> ItemsUsedOn { get; set; }


        
    }

    public class ObjectStateTracker
    {
        public ObjectStateTracker(Object targetObject)
        {
            TargetObject = targetObject;
            EffectsRemoved = new List<IEffect>();
            EffectsAdded = new List<IEffect>();
        }

        public ObjectStateTracker(int damage, AttackType type, int resist, Object targetedObject) : this(targetedObject)
        {
            Damage = damage;
            AttackType = type;
            ResistedDamage = resist;
        }

        public ObjectStateTracker(int healing, Object targetedObject) : this(targetedObject)
        {
            Healing = healing;
        }

        public bool DoesDamage { get; set; }
        public Object TargetObject { get; set; }
        public int Damage { get; set; }
        public int Healing { get; set; }
        public AttackType AttackType { get; set; }
        public int ResistedDamage { get; set; }
        public bool IsDamaged { get; set; }
        public bool IsDestroyed { get; set; }
        public List<IEffect> EffectsAdded { get; set; }
        public List<IEffect> EffectsRemoved { get; set; }
    }
}
