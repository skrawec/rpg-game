﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class AttackAction : IAction
    {
        public AttackAction(Creature currentCombatant)
        {
            SubActions = new List<SubAction>();
            _user = currentCombatant;
            GetSubActions();
        }

        private void GetSubActions()
        {
            foreach (var attack in _user.GetUseableWeapons())
            {
                SubActions.Add(new AttackSubAction(attack.Key, attack.Value, _user));
            }          
        }

        private Creature _user;

        #region IAction Members

        public List<SubAction> SubActions { get; private set; }
        public string Name { get { return "Attack"; } }


        public bool IsUseable()
        {
            bool usable = false;

            if (_user.GetUseableWeapons().Count > 0 &&
                !_user.IsDisabled)
            {
                usable = true;
            }

            return usable;
        } 

        #endregion


    }
}
