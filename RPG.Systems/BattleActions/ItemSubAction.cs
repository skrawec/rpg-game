﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class ItemSubAction : SubAction
    {
        public ItemSubAction(string name, IUseable item)
        {
            Name = name;
            _item = item;
            DefaultTargetsEnemies = false;            
        }

        private IUseable _item;

        #region ISubAction Members

        public override void ExecuteAction(TurnTracker turnTracker, Random random)
        {
            turnTracker.ActionUsed = this;

            var targetCreature = _item as ITargetCreature;
            if (targetCreature != null)
            {
                targetCreature.Use(turnTracker, Target, random);
            }

            var targetLimb = _item as ITargetLimb;
            if (targetLimb != null)
            {
                targetLimb.Use(turnTracker, TargetLimb, random);
            }

            var targetAll = _item as ITargetAll;
            if (targetAll != null)
            {
                targetAll.Use(turnTracker, GroupTarget, random);
            }

            var target = _item as ITarget;
            if (target != null)
            {
                target.Use(turnTracker, random);
            }
        }

        #endregion
    }
}
