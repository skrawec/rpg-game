﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class ItemAction : IAction
    {
        public ItemAction(Creature currentCombatant)
        {
            SubActions = new List<SubAction>();
            _user = currentCombatant;
            GetSubActions();
        }

        private void GetSubActions()
        {            
            foreach (var item in _user.Items.Where(item => item is IUseable))
            {
                ItemSubAction itemSubAction = new ItemSubAction(item.WorldlyName, (IUseable)item);

                if (item is ITargetLimb)
                    itemSubAction.LimbTarget = true;                                    

                SubActions.Add(itemSubAction);
            }
        }

        private Creature _user;

        #region IAction Members

        public List<SubAction> SubActions { get; private set; }
        public string Name { get { return "Item"; } }

        public bool IsUseable()
        {
            var useableItems = _user.Items.Where(item => item is IUseable);

            return useableItems.Count() != 0;
        } 

        #endregion
    }
}
