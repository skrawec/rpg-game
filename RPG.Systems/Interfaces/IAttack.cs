﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public interface IAttack 
    {
        //List<AttackType> AttackTypes { get; set; }
        AttackType AttackType { get; set; }
        decimal CalcDamage(Random random);
        decimal GetResistValue();
        bool IsResistable { get; }
    }
}
