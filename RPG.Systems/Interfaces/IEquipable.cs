﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public interface IEquipable
    {
        void Equip(Weapon weapon);
        void Equip(Armor armor);
        void DeEquipArmor();
        void DeEquipWeapon();
        Armor TryEquip(Armor armor);
        Weapon TryEquip(Weapon weapon);
        Equipment GetEquip();
    }
}
