﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public interface IHasVolume
    {
        decimal Volume { get; }
    }
}
