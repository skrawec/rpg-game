﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public enum Location
    {
        Volcano,
        Grassland,
        Forest,
        City,
        Ruins,
        Cave,
        House,
        Desert,
        Marsh,
        Swamp,
        Jungle,
        Mountain,
        Hill,
        //etc...
    }
}
