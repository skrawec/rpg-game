﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class Region : WorldObject, IHasArea
    {
        public Region(string name) : base(name)
        {
            
        }
        public List<Biome> Biomes { get; set; }
        
        public decimal GetArea()
        {
            return Biomes.Sum(x => x.GetArea());
        }
    }
}
