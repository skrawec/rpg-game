﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Landmass : WorldObject, IHasArea
    {
        public Landmass(string name) : base(name)
        {

        }
        public decimal Length { get; set; }
        public decimal Width { get; set; }

        public decimal GetArea()
        {
            return Length * Width;
        }
    }
}
