﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class Biome : WorldObject, IHasArea
    {
        public Biome(string name) : base(name)
        {

        }
        public List<City> Cities { get; set; }
        public List<PointOfInterest> PointsOfInterest { get; set; }

        public decimal GetArea()
        {
            return Cities.Sum(city => city.GetArea()) + PointsOfInterest.Sum(poi => poi.GetArea());
        }
    }
}
