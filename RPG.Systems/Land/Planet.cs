﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Planet : WorldObject, IHasArea
    {
        public Planet(string name) : base(name)
        {

        }
        public List<Continent> Continents { get; set; }        

        public decimal GetArea()
        {
            return Continents.Sum(con => con.GetArea());
        }
    }
}
