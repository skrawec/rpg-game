﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class City : Landmass
    {
        public City(string name) : base(name)
        {

        }
        public List<Denizen> Denizens { get; set; }
        
        public int TotalPopulation
        {
            get { return Denizens.Sum(x => x.Population); }
        }
        
    }
}
