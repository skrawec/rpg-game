﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class Denizen : WorldObject
    {
        public Denizen(string name) : base(name)
        {

        }
        public Creature Creature { get; set; }
        public int Population { get; set; }
    }
}
