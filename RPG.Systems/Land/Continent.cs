﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Continent : WorldObject, IHasArea
    {
        public Continent(string name) : base(name)
        {

        }

        List<Region> Regions { get; set; }

        public decimal GetArea()
        {
            return Regions.Sum(region => region.GetArea());
        }
    }
}
