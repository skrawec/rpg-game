﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class HealingTouch : IManifestation, ITargetLimb
    {
        public HealingTouch(Creature user, int baseHealing)
        {
            _baseHealing = baseHealing;
            User = user;
            Name = "HealingTouch";
            StrainCost = 10;
            DefaultTargetEnemies = false;
            AutoTarget = false;
            LimbTarget = true;
                
        }

        private int _baseHealing;
        public Creature User { get; private set; }

        #region IManifestation Members

        public bool AutoTarget { get; private set; }
        public bool LimbTarget { get; private set; }
        public bool DefaultTargetEnemies { get; private set; }
        public string Name { get; private set; }
        public int StrainCost { get; private set; }

        #endregion

        #region ITargetLimb

        public void Use(TurnTracker turnTracker, Limb limb, Random random)
        {
            decimal totalHealing = _baseHealing * User.Intelligence;
            int curHPdifference = limb.MaxDurablity - limb.CurrentDurability;

            if (totalHealing > curHPdifference)
            {
                limb.CurrentDurability = limb.MaxDurablity;
                totalHealing = curHPdifference;
            }
            else
            {
                limb.CurrentDurability += (int)decimal.Round(totalHealing, 0);
            }

            ObjectStateTracker objectDamageTracker = new ObjectStateTracker((int)Math.Abs(totalHealing), limb);            
            TargetTracker targetTracker = new TargetTracker(limb.LimbOwner);
            turnTracker.Targets.Add(targetTracker);
            

            //String.Format("{0} used on {1}'s {2} and healed {3} HP.", Name, limb.LimbOwner.Name, limb.WorldlyName, totalHealing);
        } 

        #endregion
    }
}
