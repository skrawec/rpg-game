﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public interface IManifestation
    {
        bool AutoTarget { get; }
        bool LimbTarget { get; }
        bool DefaultTargetEnemies { get; }
        string Name { get; }
        int StrainCost { get; }
        Creature User { get; }
    }
}
