﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Fire : IManifestation, ITargetCreature, IAttack
    {
        public Fire(int baseDamage, Creature user)
        {
            _baseDamage = baseDamage;
            User = user;
            AttackType = AttackType.Fire;
            _modifier = 1M;
            StrainCost = 4;
            Name = "Fire";
            AutoTarget = false;
            LimbTarget = false;
            DefaultTargetEnemies = true;
            IsResistable = true;
        }

        private int _baseDamage;
        public Creature User { get; private set; }
        private decimal _modifier;

        #region ITargetCreature Members

        public void Use(TurnTracker turnTracker, Creature target, Random random)
        {            
            Object targetedObject = Targeting.RandomLimb(target, random);
            targetedObject.Damage(turnTracker, this, random);

            _modifier = 0.3M;
            if (targetedObject is Limb)
            {
                List<Limb> adjacentLimbs = (targetedObject as Limb).GetAdjacentLimbs();
                
                foreach (var adjacentLimb in adjacentLimbs)
                {
                    adjacentLimb.Damage(turnTracker, this, random);
                }

                
            }
            else if (targetedObject is Piece)
            {
                if ((targetedObject as Piece).EquipmentOwner is Weapon)
                {
                    IEnumerable<Piece> otherPieces = ((targetedObject as Piece).EquipmentOwner as Weapon).AttachedPieces.Where(x => x != (targetedObject as Piece));

                    foreach (var piece in otherPieces)
                    {
                        piece.Damage(turnTracker, this, random);
                    }
                }                
            }

            _modifier = 1M;
        }

        #endregion

        #region IManifestation Members

        public bool AutoTarget { get; private set; }
        public bool LimbTarget { get; private set; }
        public bool DefaultTargetEnemies { get; private set; }
        public string Name { get; private set; }
        public int StrainCost { get; private set; }

        #endregion

        #region IAttack Members

        public AttackType AttackType { get; set; }

        public decimal CalcDamage(Random random)
        {
            return _baseDamage * _modifier * (User.Intelligence / 10);
        }

        public decimal GetResistValue()
        {
            return 5;
        }

        public bool IsResistable { get; private set; }

        #endregion
    }
}
