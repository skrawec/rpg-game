﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public abstract class Object : WorldObject, IHasVolume, IDamagable
    {        
        public Object(string name, Material material, I3DShape shape, bool isSolid, int? curDur = null) : base(name)
        {            
            Material = material;
            Shape = shape;
            IsSolid = isSolid;

            CurrentDurability = curDur == null ? MaxDurablity : curDur.Value;
        }

        public I3DShape Shape { get; set; }

        public Material Material { get; set; }

        public int CurrentDurability { get; set; }

        public int MaxDurablity
        {
            get
            {
                //Tiers of HP                
                int durability = 0;

                durability = (int)(Math.Abs(Volume) * Material.Hardness / 100);

                if (durability < 10)
                    durability = 10;

                if (!IsSolid)
                    durability = durability / 10;

                return durability;
            }
        } 
        

        public bool IsDamaged { get; set; }

        public bool IsDestroyed { get; set; }

        public int DestroyedPoint 
        {   
            get { return (MaxDurablity) * (-1); } //Simple for now, negative half the max durability
        }

        public bool IsSolid { get; set; }

        public decimal Weight
        {
            get { return Shape.CalcVolume() * Material.Density; }
        }

        public decimal Volume
        { 
            get 
            { 
                decimal volume = Shape.CalcVolume();

                //if not solid then take only 20% of the volume
                if (!IsSolid)
                    volume = volume * 0.5M;

                return volume;
            }
        }

        public decimal Worth
        {
            get { return Weight * Shape.CalcVolume() * Material.Worth; }
            
        }

        /// <summary>
        /// Take Damage on the object
        /// </summary>
        /// <param name="weapon">Incoming Weapon</param>
        public abstract void Damage(TurnTracker turnTracker, IAttack attack, Random random);
  
        internal virtual void DamagedHandler(ObjectStateTracker objectDamageTracker, IAttack attack)
        {
            objectDamageTracker.IsDamaged = true;
            IsDamaged = true;
        }
  
        internal virtual void DestroyedHandler(ObjectStateTracker objectDamageTracker, IAttack attack)
        {
            objectDamageTracker.IsDestroyed = true;
            IsDestroyed = true;
        }

        public string GetDestroyedWord(Random random)
        {
            //Get random destroyed Word                    
            int randomNumber = random.Next(0, Material.DestroyedWords.Count - 1);
            return this.Material.DestroyedWords[randomNumber];
        }

        /// <summary>
        /// Calculate the objects full resistance
        /// </summary>
        /// <returns></returns>
        internal decimal CalcResistance()
        {
            //Get object's Depth
            decimal weight = Weight;
            //If the object is not solid only take 20% of the depth
            if (!IsSolid)
                weight = weight * 0.5M;

            //return the full materials resistance
            return Material.Hardness * (weight / 1000);
        }

        internal decimal CalcTotalResistance(IAttack attack)
        {
            //Compare object hardnesses
            decimal hardnessDifference = Material.Hardness / attack.GetResistValue();
            //Multiply the percentage difference in hardness to the resistance total
            decimal resistance = CalcResistance();
            decimal resistModifier = 1.0M;

            //if the weapontype is found in the weakness reduce resistance by 25%
            if (Material.WeakTypes.Contains(attack.AttackType))
            {
                resistModifier -= 0.25M;
            } //add 25% more resistance if the weaponType is resisted
            else if (Material.ResistanceTypes.Contains(attack.AttackType))
            {
                resistModifier += 0.25M;
            }

            return resistance * resistModifier * hardnessDifference;
        }

        internal int CalcTotalDamage(decimal incomingWeaponDamage, decimal totalResistance)
        {
            //calculate potential damage
            decimal potentialDamage = incomingWeaponDamage - totalResistance;
            //if potential damage is 0 or below it's fully resisted and it's set to 0
            int totalDamage = (int)decimal.Round(potentialDamage >= 0 ? potentialDamage : 0, 0);
            return totalDamage;
        }

        public override string ToString()
        {
            return WorldlyName;
        }
    }
}
