﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Equipment : Item
    {
        public Equipment(string name) : base(name)
        {
            Name = name;
        }

        public Limb LimbEquippedOn { get; set; }

        public Creature Weilder { get; set; }

        public string Name { get; internal set; }
    }
}
