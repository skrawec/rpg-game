﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class Piece : Object
    {
        public Piece(string name, Material material, I3DShape shape, bool isSolid, bool isWeaponPiece, bool isHandle = false, int? curDur = null) : base(name, material, shape, isSolid, curDur)
        {
            IsWeaponPiece = isWeaponPiece;
        }

        public string Name
        {
            get { return Material.WorldlyName + " " + WorldlyName; }
        }

        public bool IsWeaponPiece { get; set; }
        public bool IsHandle { get; set; }
        public Equipment EquipmentOwner { get; set; }

        internal int GetMaxArmorPoints()
        {
            return MaxDurablity;
        }

        internal int GetCurrentArmorPoints()
        {
            return CurrentDurability;
        }

        public override void Damage(TurnTracker turnTracker, IAttack attack, Random random)
        {            
            //get the incoming weapon damage
            decimal incomingWeaponDamage = attack.CalcDamage(random);

            decimal totalResistance = CalcTotalResistance(attack);

            int totalDamage = CalcTotalDamage(incomingWeaponDamage, totalResistance);

            int totalResisted = (int)incomingWeaponDamage - totalDamage;

            //finally reduce durablity by the rounded total damage
            CurrentDurability -= totalDamage;

            //Setup trackers                       
            Creature wielder = EquipmentOwner.LimbEquippedOn == null ? null : EquipmentOwner.LimbEquippedOn.LimbOwner;


            ObjectStateTracker objectStateTracker = new ObjectStateTracker(totalDamage, attack.AttackType, totalResisted, this);
            objectStateTracker.DoesDamage = true;
            TargetTracker targetTracker = new TargetTracker(wielder, objectStateTracker);

            turnTracker.Targets.Add(targetTracker);

            if (CurrentDurability < DestroyedPoint)
            {
                DestroyedHandler(objectStateTracker, attack);               
            }
            else if (CurrentDurability < 1)
            {
                DamagedHandler(objectStateTracker, attack);

                if (IsHandle)
                {
                    targetTracker.EquipmentDropped.Add(EquipmentOwner);
                    DeEquipEquipment();                                        
                }
                else
                {
                    if (EquipmentOwner is Weapon)
                    {
                        var weapon = EquipmentOwner as Weapon;

                        weapon.AttachedPieces.Remove(this);
                        weapon.UpdateName(string.Format("Broken {0}", weapon.Name));
                    }
                }
            }
        }
  
        internal override void DamagedHandler(ObjectStateTracker objectStateTracker, IAttack attack)
        {
            IsDamaged = true;
            objectStateTracker.IsDamaged = true;
        }
  
        internal override void DestroyedHandler(ObjectStateTracker objectStateTracker, IAttack attack)
        {
            IsDestroyed = true;
            objectStateTracker.IsDestroyed = true;

        }

        void DeEquipEquipment()
        {
            if (EquipmentOwner is Weapon && EquipmentOwner.LimbEquippedOn != null)
                EquipmentOwner.LimbEquippedOn.DeEquipWeapon();
            else if (EquipmentOwner is Armor && EquipmentOwner.LimbEquippedOn != null)
                EquipmentOwner.LimbEquippedOn.DeEquipArmor();
        }
    }
}
