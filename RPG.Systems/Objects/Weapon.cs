﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Weapon : Equipment, IAttack
    {
        public Weapon(string name) : base(name)
        {            
            AttachedPieces = new List<Piece>();
        }

        public decimal Weight
        {
            get { return AttachedPieces.Sum(x => x.Weight); }
        }

        public Object WeaponPiece
        {
            get { return AttachedPieces.FirstOrDefault(x => x.IsWeaponPiece); }
            
        }


        public List<Piece> AttachedPieces { get; set; }

        public AttackType AttackType { get; set; }

        public decimal CalcDamage(Random random)
        {
            return CombatHelper.CalcDamage(random, this, Weilder); 
        }

        public decimal GetResistValue()
        {
            return CombatHelper.CalcResist(this);            
        }


        public bool IsResistable { get { return true; } }

        internal void UpdateName(string newName)
        {
            Name = newName;
        }
    }
}
