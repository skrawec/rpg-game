﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class Material : WorldObject
    {        
        public Material(string name) : base(name)
        {
            ResistanceTypes = new List<AttackType>();
            WeakTypes = new List<AttackType>();
            DestroyedWords = new List<string>();
        }

        public bool IsOrganic { get; set; }
        public List<string> DestroyedWords { get; set; }
        public decimal Worth { get; set; }
        public decimal Density { get; set; }
        public int Resist { get; set; }
        public List<AttackType> ResistanceTypes { get; set; }
        public List<AttackType> WeakTypes { get; set; }
        public decimal Hardness { get; set; } //Mohs Scale? 1 = talc, 4 = copper, 4.5 = steel, 7 = glass, 8 = hardended steel, 10 = diamond 

        //More Techncial?
        public decimal MeltingPoint { get; set; }
        public decimal BoilingPoint { get; set; }
        public decimal YieldStrength { get; set; }
        public decimal UlitmateStrength { get; set; }
        
    }
}
