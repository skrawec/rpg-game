﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class Armor : Equipment
    {
        public Armor(string name) : base(name) { }

        public Piece ArmorPiece { get; set; }
    }
}
