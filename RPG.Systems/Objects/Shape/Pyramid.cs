﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Pyramid : I3DShape
    {
        public IShape Base { get; set; }
        public decimal Height { get; set; }

        public decimal CalcVolume()
        {
            return (1 / 3) * Base.CalcArea() * Height;
        }

        public decimal CalcHeight()
        {
            return Height;
        }
    }
}
