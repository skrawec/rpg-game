﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems.Objects.Shape
{
    public class Hexagon : IShape
    {
        public double SideLength { get; set; }

        public decimal CalcArea()
        {
            return ((3*(decimal)Math.Sqrt(3)) * (decimal)Math.Pow(SideLength,2)) /2;
        }
    }
}
