﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Rectangle : IShape
    {
        public decimal Length { get; set; }
        public decimal Width { get; set; }

        public decimal CalcArea()
        {
            return Length * Width;
        }
    }
}
