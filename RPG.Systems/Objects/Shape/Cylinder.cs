﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Cylinder : I3DShape
    {
        public double Radius { get; set; }
        public decimal Height { get; set; }

        public decimal CalcVolume()
        {
            return (decimal)Math.PI * (decimal)Math.Pow(Radius, 2) * Height;
        }

        public decimal CalcHeight()
        {
            return Height;
        }
    }
}
