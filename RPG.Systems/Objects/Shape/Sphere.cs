﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Sphere : I3DShape
    {
        public double Radius { get; set; }

        public decimal CalcVolume()
        {
            return 4 * (decimal)Math.PI * (decimal)Math.Pow(Radius, 2);
        }

        public decimal CalcHeight()
        {
            return (decimal)Radius * 2;
        }
    }
}
