﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Cuboid : I3DShape
    {
        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public decimal Depth { get; set; }

        public decimal CalcVolume()
        {
            return Height * Width * Depth;
        }

        public decimal CalcHeight()
        {
            return Height;
        }
    }
}
