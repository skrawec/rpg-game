﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public interface I3DShape
    {
        decimal CalcVolume();
        decimal CalcHeight();
    }
}
