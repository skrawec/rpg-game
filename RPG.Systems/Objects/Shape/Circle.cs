﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Circle : IShape
    {
        public double Radius { get; set; }

        public decimal CalcArea()
        {
            return (decimal)Math.PI * (decimal)Math.Pow(Radius,2);
        }
    }
}
