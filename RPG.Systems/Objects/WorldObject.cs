﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public abstract class WorldObject
    {
        public WorldObject(string name)
        {
            WorldlyName = name;
        }

        public int ID { get; set; }
        public virtual string WorldlyName { get; private set; }
    }
}
