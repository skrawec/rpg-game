﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Bandage : Item, IUseable, ITargetLimb
    {
        public Bandage(Creature owner) : base("Bandage")
        {
            _owner = owner;   
        }

        private Creature _owner;

        public void Use(TurnTracker turnTracker, Limb limb, Random random)
        {
            if (limb == null)
                throw new Exception("Limb cannot be null.");

            limb.Effects.RemoveAll(effect => effect is Bleeding);

            TargetTracker targetTracker = new TargetTracker(limb.LimbOwner);
            targetTracker.ItemsUsedOn.Add(this);

            turnTracker.Targets.Add(targetTracker);
            //string output = string.Format("{0} has bandaged {1}'s {2} and stopped the bleeding.\n", _owner.Name, limb.LimbOwner.Name, limb.Name);

            _owner.Items.Remove(this);

            //return output;
        }

        public string Name
        {
            get { return WorldlyName; }
        }
    }
}
