﻿using System;
using System.Collections.Generic;

namespace RPG.Systems
{
    internal interface IResistableEffect
    {
        bool TryResist(Random random);
        Severity EffectSeverity{ get; }
        EffectType EffectType { get; }
    }
}