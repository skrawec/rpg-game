﻿using System;

namespace RPG.Systems
{
    public interface ISpreadableEffect
    {
        void TrySpreading(TurnTracker turnTracker, Random random);
        double SpreadChance { get; }
        int SpreadSpeed { get; }
        int CurrentSpreadSpeed { get; }
        bool HasJustSpread { get; set; }
        
    }
}