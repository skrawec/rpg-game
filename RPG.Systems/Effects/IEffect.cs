﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public interface IEffect
    {
        void Apply(TargetTracker targetTracker, Limb limb);
        void OnGoing(TurnTracker turnTracker, Random random);
        void ExitEffect(ObjectStateTracker objectDamageTracker);
        string Name { get;  }
        bool ToRemove { get; }
        Guid EffectGuid { get; }
    }
}
