﻿namespace RPG.Systems
{
    public interface IStackableEffect
    {
        void StackEffect(TargetTracker targetTracker);
    }
}