﻿namespace RPG.Systems
{
    public enum Severity
    {
        None,
        Low,
        Medium,
        High,
        Critical
    }
}