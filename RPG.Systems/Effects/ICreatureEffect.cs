﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public interface ICreatureEffect
    {
        void Apply(TargetTracker targetTracker);
        void OnGoing(TurnTracker turnTracker, Random random);
        void ExitEffect(TurnTracker turnTracker);
        string Name { get; }
        bool ToRemove { get; }
        Guid EffectGuid { get; }
    }
}
