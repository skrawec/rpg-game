﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems.Effects
{
    public class OffBalance : ICreatureEffect, IStackableEffect, IHaveLifeTimeEffect
    {
        public OffBalance(Creature creature, int turns)
        {
            _creature = creature;
            Turns = turns;
            CurrentTurns = turns;
            ToRemove = false;
            Name = "Stunned";
        }

        private Creature _creature;
        public int CurrentTurns { get; private set; }
        public int Turns { get; private set; }
        public bool ToRemove { get; private set; }
        public string Name { get; private set; }
        public Guid EffectGuid { get; private set; }

        public void Apply(TargetTracker targetTracker)
        {
            //TODO: Change this, this cannot work if other things disabled the creature and this would remove it.
            //TODO: Needs to be a system where the creature checks for disabling effects
            _creature.IsDisabled = true;
            if (_creature.Effects.Any(effect => effect is OffBalance))
            {
                (_creature.Effects.FirstOrDefault(effect => effect is OffBalance) as OffBalance).StackEffect(targetTracker);
            }
            else
            {
                _creature.Effects.Add(this);
                targetTracker.EffectsAdded.Add(this);
            }
        }

        public void OnGoing(TurnTracker turnTracker, Random random)
        {
            if (CurrentTurns == 0)
            {
                ToRemove = true;
            }

            CurrentTurns--;
        }


        public void ExitEffect(TurnTracker turnTracker)
        {
            TargetTracker targetTracker = new TargetTracker(_creature);
            turnTracker.Targets.Add(targetTracker);
            //TODO: Same as above TODO
            _creature.IsDisabled = false;
            targetTracker.EffectsRemoved.Add(this);
        }

        public void StackEffect(TargetTracker targetTracker)
        {
            targetTracker.EffectsExtended.Add(this);
            CurrentTurns += Turns;
        }
    }
}
