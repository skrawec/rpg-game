﻿namespace RPG.Systems
{
    public interface IHaveLifeTimeEffect
    {
        int Turns { get; }
        int CurrentTurns { get; }
    }
}