﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Bleeding : IEffect, IAttack
    {
        public Bleeding(Limb limb, int strength)
        {
            _limb = limb;
                        
            AttackType = AttackType.Bleeding;
            _bleedStrength = strength;
            ToRemove = false;
            Name = "Bleeding";
        }

        private int _bleedStrength;
        private Limb _limb;
        public Guid EffectGuid { get; private set; }
        public bool ToRemove { get; private set; }
        public string Name { get; private set; }

        public void Apply(TargetTracker targetTracker, Limb limb)
        {
            limb.Effects.Add(this);

            

            ObjectStateTracker objectStateTracker = new ObjectStateTracker(limb);
            objectStateTracker.EffectsAdded.Add(this);
            objectStateTracker.DoesDamage = false;
            targetTracker.ObjectStateTrackers.Add(objectStateTracker);
        }

        public void OnGoing(TurnTracker turnTracker, Random random)
        {            
            if (_limb.CurrentDurability == 0)
            {
                ToRemove = true;
            }
            else
            {
                _limb.Damage(turnTracker, this, random);
            }
        }

        public AttackType AttackType { get; set; }

        public decimal CalcDamage(Random random)
        {
            return _bleedStrength;
        }

        public decimal GetResistValue()
        {
            return 0;
        }

        public bool IsResistable { get { return false; } }

        

        public void ExitEffect(ObjectStateTracker objectStateTracker)
        {
            objectStateTracker.EffectsRemoved.Add(this);
            objectStateTracker.DoesDamage = false;
        }
    }
}
