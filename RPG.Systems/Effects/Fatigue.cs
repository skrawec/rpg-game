﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Fatigue : ICreatureEffect
    {
        public Fatigue(Creature creature)
        {
            _creature = creature;
            ToRemove = false;
            Name = "Fatigued";
        }

        private Creature _creature;

        public void Apply(TargetTracker targetTracker)
        {
            //reduce stats
            _creature.Effects.Add(this);
            targetTracker.EffectsAdded.Add(this);
        }

        public void OnGoing(TurnTracker turnTracker, Random random)
        {
            //TODO add the effect
        }

        public void ExitEffect(TurnTracker turnTracker)
        {
            TargetTracker targetTracker = new TargetTracker(_creature);
            turnTracker.Targets.Add(targetTracker);

            //return stats to original

            ToRemove = true;
            targetTracker.EffectsRemoved.Add(this);
        }

        public string Name { get; private set; }
        public Guid EffectGuid { get; private set; }

        public bool ToRemove { get; private set; }
    }
}
