﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Prone : ICreatureEffect
    {
        public Prone(Creature creature)
        {
            _creature = creature;
            Name = "Prone";
        }

        private Creature _creature;

        public void Apply(TargetTracker targetTracker)
        {
            //apply accuracy penalty for melee weapons
        }

        public void OnGoing(TurnTracker turnTracker, Random random)
        {
            //not sure what to put here
        }

        public void ExitEffect(TurnTracker turnTracker)
        {
            TargetTracker targetTracker = new TargetTracker(_creature);
            turnTracker.Targets.Add(targetTracker);
            //remove accuracy penalty for melee weapons
        }

        public string Name { get; private set; }

        public bool ToRemove { get; private set; }
        public Guid EffectGuid { get; private set; }
    }
}
