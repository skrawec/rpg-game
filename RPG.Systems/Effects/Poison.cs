﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Poison : IEffect, IAttack, IResistableEffect, ISpreadableEffect
    {
        private Limb _limb;

        public Poison(string poisonName, Severity effectSeverity, double spreadChance, int spreadSpeed, bool justSpread = false)
            : this(poisonName, effectSeverity, spreadChance, spreadSpeed, Guid.NewGuid(), justSpread)
        { }

        public Poison(string poisonName, Severity effectSeverity, double spreadChance, int spreadSpeed, Guid effectGuid, bool justSpread = false)
        {
            EffectGuid = effectGuid;
            AttackType = AttackType.Poison;
            SpreadSpeed = spreadSpeed;
            CurrentSpreadSpeed = spreadSpeed;
            EffectSeverity = effectSeverity;
            SpreadChance = spreadChance;
            ToRemove = false;
            EffectType = EffectType.Poison;
            Name = poisonName;
            HasJustSpread = justSpread;
            IsResistable = false;
        }

        public bool HasJustSpread { get; set; }
        public int SpreadSpeed { get; private set; }
        public int CurrentSpreadSpeed { get; private set; }        
        public double SpreadChance { get; private set; }
        public Guid EffectGuid { get; private set; }
        public EffectType EffectType { get; private set; }
        public bool ToRemove { get; private set; }
        public string Name { get; private set; }
        public Severity EffectSeverity { get; private set; }
        public bool IsResistable { get; private set; }

        public void Apply(TargetTracker targetTracker, Limb limb)
        {
            limb.Effects.Add(this);
            _limb = limb;

            ObjectStateTracker objectStateTracker = new ObjectStateTracker(limb);
            objectStateTracker.EffectsAdded.Add(this);
            objectStateTracker.DoesDamage = false;

            targetTracker.ObjectStateTrackers.Add(objectStateTracker);
        }

        public void OnGoing(TurnTracker turnTracker, Random random)
        {
            if (_limb.LimbType is UpperBody)
                _limb.Damage(turnTracker, this, random);
        }

        public AttackType AttackType { get; set; }

        public decimal CalcDamage(Random random)
        {
            return (int)EffectSeverity;
        }

        public decimal GetResistValue()
        {
            return 0;
        }

        public void ExitEffect(ObjectStateTracker objectStateTracker)
        {
            objectStateTracker.EffectsRemoved.Add(this);
            objectStateTracker.DoesDamage = false;
        }

        public bool TryResist(Random random)
        {            
            if (_limb.LimbOwner.Resist(this, random))
            {
                ToRemove = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void TrySpreading(TurnTracker turnTracker, Random random)
        {
            if (!HasJustSpread)             
            {
                if (CurrentSpreadSpeed == 0)
                {
                    //Check if top limb
                    if (_limb.ParentLimb != null)
                    {
                        //Check if parentLimb already has this effect
                        if (!_limb.ParentLimb.Effects.Any(y => y.EffectGuid == EffectGuid))
                        {
                            //roll the dice
                            double percentRole = random.NextDouble();
                            //if the poison spread chance beats the percent role apply poison
                            if (SpreadChance > percentRole)
                            {
                                var poison = new Poison(Name, EffectSeverity, SpreadChance, SpreadSpeed, EffectGuid, true);
                                TargetTracker targetTracker = new TargetTracker(_limb.LimbOwner);
                                turnTracker.Targets.Add(targetTracker);
                                poison.Apply(targetTracker, _limb.ParentLimb);

                                //Remove posion from this limb now that it's in parent limb
                                ToRemove = true;
                            }
                        }
                    }

                    CurrentSpreadSpeed = SpreadSpeed + 1; //+1 cause it's minused right away
                }

                CurrentSpreadSpeed--;
            }
        }
    }
}
