﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class GenerateCreature
    {
        public static Creature Humanoid(string name, Gender gender, int agility, int strength, int intelligence, int wisdom, int constitution)
        {
            Creature creature = new Humanoid(name, strength, intelligence, agility, wisdom, constitution, 10);

            CreateCreature(gender, agility, strength, intelligence, wisdom, constitution, creature);

            return creature;
        }

        public static Player Player(string name, Gender gender, int agility, int strength, int intelligence, int wisdom, int constitution)
        {   
            Player creature = new Player(name, strength, intelligence, agility, wisdom, constitution, 10);

            CreateCreature(gender, agility, strength, intelligence, wisdom, constitution, creature);

            return creature;
        }

        private static void CreateCreature(Gender gender, int agility, int strength, int intelligence, int wisdom, int constitution, Creature creature)
        {
            Limb Chest = GenerateLimbs(creature);

            creature.Limbs.Add(Chest);
            
            creature.Stamina = 10;
            creature.Gender = gender;
            creature.Agility = agility;
            creature.Strength = strength;
            creature.Intelligence = intelligence;
            creature.Wisdom = wisdom;
            creature.CurrentMorale = creature.MaxMorale;
            creature.Constitution = constitution;
        }

        private static Limb GenerateLimbs(Creature creature)
        {
            Material skin = GenerateMaterial.Skin;


            Limb head = new Limb("Head", creature, true, skin, new Sphere() { Radius = 8 });
            head.LimbType = new Head(head);

            Limb neck = new Limb("Neck", creature, true, skin, new Cylinder() { Height = 6, Radius = 3 });
            neck.AttachLimb(head);
            neck.LimbType = new Neck(neck);

            Limb rightHand = new Limb("Hand", creature, false, skin, new Cuboid() { Depth = 10, Height = 2, Width = 6 }, Side.Right);
            Limb leftHand = new Limb("Hand", creature, false, skin, new Cuboid() { Depth = 10, Height = 2, Width = 6 }, Side.Left);
            rightHand.LimbType = new Hand(rightHand);
            rightHand.IsWeaponMountable = true;
            leftHand.LimbType = new Hand(leftHand);
            leftHand.IsWeaponMountable = true;

            Limb leftForeArm = new Limb("ForeArm", creature, false, skin, new Cylinder() { Radius = 3, Height = 25 }, Side.Left);
            leftForeArm.LimbType = new Arm(leftForeArm);
            leftForeArm.AttachLimb(leftHand);

            Limb rightForeArm = new Limb("ForeArm", creature, false, skin, new Cylinder() { Radius = 3, Height = 25 }, Side.Right);
            rightForeArm.LimbType = new Arm(rightForeArm);
            rightForeArm.AttachLimb(rightHand);

            Limb leftArm = new Limb("Arm", creature, false, skin, new Cylinder() { Radius = 3.5, Height = 24 }, Side.Left);
            leftArm.LimbType = new Arm(leftArm);
            leftArm.AttachLimb(leftForeArm);

            Limb leftShoulder = new Limb("Shoulder", creature, false, skin, new Sphere() { Radius = 4 }, Side.Left);
            leftShoulder.LimbType = new Arm(leftShoulder);
            leftShoulder.AttachLimb(leftArm);

            Limb rightArm = new Limb("Arm", creature, false, skin, new Cylinder() { Radius = 3.5, Height = 24 }, Side.Right);
            rightArm.LimbType = new Arm(rightArm);
            rightArm.AttachLimb(rightForeArm);

            Limb rightShoulder = new Limb("Shoulder", creature, false, skin, new Sphere() { Radius = 4 }, Side.Right);
            rightShoulder.LimbType = new Arm(rightShoulder);
            rightShoulder.AttachLimb(rightArm);

            Limb rightFoot = new Limb("Foot", creature, false, skin, new Cuboid() { Width = 7, Height = 5, Depth = 20 }, Side.Right);
            rightFoot.LimbType = new Leg(rightFoot);

            Limb leftFoot = new Limb("Foot", creature, false, skin, new Cuboid() { Width = 7, Height = 5, Depth = 20 }, Side.Left);
            leftFoot.LimbType = new Leg(leftFoot);

            Limb rightLeg = new Limb("Leg", creature, false, skin, new Cylinder() { Radius = 3.5, Height = 30 }, Side.Right);
            rightLeg.LimbType = new Leg(rightLeg);
            rightLeg.AttachLimb(rightFoot);

            Limb leftLeg = new Limb("Leg", creature, false, skin, new Cylinder() { Radius = 3.5, Height = 30 }, Side.Left);
            leftLeg.LimbType = new Leg(leftLeg);
            leftLeg.AttachLimb(leftFoot);

            Limb rightThigh = new Limb("Thigh", creature, false, skin, new Cylinder() { Radius = 5, Height = 25 }, Side.Right);
            rightThigh.LimbType = new Leg(rightThigh);
            rightThigh.AttachLimb(rightLeg);

            Limb leftThigh = new Limb("Thigh", creature, false, skin, new Cylinder() { Radius = 5, Height = 25 }, Side.Left);
            leftThigh.LimbType = new Leg(leftThigh);
            leftThigh.AttachLimb(leftLeg);

            Limb lowerTorso = new Limb("Lower Torso", creature, true, skin, new Cuboid() { Depth = 10, Height = 10, Width = 20 });
            lowerTorso.LimbType = new UpperBody(lowerTorso);
            lowerTorso.AttachLimb(leftThigh);
            lowerTorso.AttachLimb(rightThigh);

            Limb upperTorso = new Limb("Upper Torso", creature, true, skin, new Cuboid() { Depth = 10, Height = 20, Width = 20 });
            
            upperTorso.LimbType = new UpperBody(upperTorso);
            upperTorso.AttachLimb(neck);
            upperTorso.AttachLimb(leftShoulder);
            upperTorso.AttachLimb(rightShoulder);
            upperTorso.AttachLimb(lowerTorso);

            return upperTorso;
        }

        public static Creature GenerateWolf(string name, int str, int intel, int agi, int wis, int con, int cha)
        {
            Creature wolf = new Animal(name, str, intel, agi, wis, con, cha);
            wolf.Gender = Gender.Unknown;

            NaturalWeaponLimb jaws = new NaturalWeaponLimb("Jaws", wolf, false, GenerateMaterial.Bone, new Cuboid() { Depth = 10, Height = 6, Width = 6 }, AttackType.Piercing, true);
            jaws.LimbType = new Misc(jaws);

            Limb head = new Limb("Head", wolf, true, GenerateMaterial.Hide, new Sphere() { Radius = 9 });
            head.AttachLimb(jaws);
            head.LimbType = new Head(head);

            Limb neck = new Limb("Neck", wolf, true, GenerateMaterial.Hide, new Cylinder() { Height = 6, Radius = 6 });
            neck.AttachLimb(head);
            neck.LimbType = new Neck(neck);            


            Limb tail = new Limb("Tail", wolf, false, GenerateMaterial.Hide, new Cylinder() { Height = 30, Radius = 1 });
            tail.LimbType = new Misc(tail); 

            Limb back = new Limb("Back", wolf, true, GenerateMaterial.Hide, new Cuboid() { Depth = 30, Height = 15, Width = 20 });
            back.LimbType = new UpperBody(back);
            back.AttachLimb(tail);            


            Limb rightHindPaw = new Limb("Hind Paw", wolf, false, GenerateMaterial.Hide, new Cuboid() { Depth = 5, Height = 5, Width = 5 }, Side.Right);
            rightHindPaw.LimbType = new Leg(rightHindPaw);

            Limb rightHindLeg = new Limb("Hind Leg", wolf, false, GenerateMaterial.Hide, new Cylinder() { Height = 30, Radius = 3.5 }, Side.Right);
            rightHindLeg.LimbType = new Leg(rightHindLeg);
            rightHindLeg.AttachLimb(rightHindPaw);

            NaturalWeaponLimb rightClaws = new NaturalWeaponLimb("Claws", wolf, false, GenerateMaterial.Bone, new Cuboid() { Depth = 2, Height = 2, Width = 4 }, AttackType.Slashing, true, Side.Right);
            rightClaws.LimbType = new Misc(rightClaws);

            Limb rightForePaw = new Limb("Fore Paw", wolf, false, GenerateMaterial.Hide, new Cuboid() { Depth = 5, Height = 5, Width = 5 }, Side.Right);
            rightForePaw.LimbType = new Leg(rightForePaw);
            rightForePaw.AttachLimb(rightClaws);

            Limb rightForearm = new Limb("Forearm", wolf, false, GenerateMaterial.Hide, new Cylinder() { Height = 30, Radius = 2.5 }, Side.Right);
            rightForearm.LimbType = new Leg(rightForearm);
            rightHindLeg.AttachLimb(rightForePaw);


            Limb leftHindPaw = new Limb("Hind Paw", wolf, false, GenerateMaterial.Hide, new Cuboid() { Depth = 5, Height = 5, Width = 5 }, Side.Left);
            leftHindPaw.LimbType = new Leg(leftHindPaw);

            Limb leftHindLeg = new Limb("Hind Leg", wolf, false, GenerateMaterial.Hide, new Cylinder() { Height = 30, Radius = 3.5 }, Side.Left);
            leftHindLeg.LimbType = new Leg(leftHindLeg);
            leftHindLeg.AttachLimb(leftHindPaw);


            NaturalWeaponLimb leftClaws = new NaturalWeaponLimb("Claws", wolf, false, GenerateMaterial.Bone, new Cuboid() { Depth = 2, Height = 2, Width = 4 }, AttackType.Slashing, true, Side.Left);
            leftClaws.LimbType = new Misc(leftClaws);
            Limb leftForePaw = new Limb("Fore Paw", wolf, false, GenerateMaterial.Hide, new Cuboid() { Depth = 5, Height = 5, Width = 5 }, Side.Left);
            leftForePaw.LimbType = new Leg(leftForePaw);
            leftForePaw.AttachLimb(leftClaws);
            

            Limb leftForearm = new Limb("Forearm", wolf, false, GenerateMaterial.Hide, new Cylinder() { Height = 30, Radius = 2.5 },  Side.Left);
            leftForearm.LimbType = new Leg(leftForearm);
            leftHindLeg.AttachLimb(leftForePaw);
            

            Limb chest = new Limb("Chest", wolf, true, GenerateMaterial.Hide, new Cuboid() { Depth = 35, Height = 15, Width = 20 });


            chest.LimbType = new UpperBody(chest);
            chest.AttachLimb(neck);
            chest.AttachLimb(back);
            chest.AttachLimb(leftHindLeg);
            chest.AttachLimb(rightHindLeg);
            chest.AttachLimb(leftForearm);
            chest.AttachLimb(rightForearm);

            wolf.Limbs.Add(chest);

            return wolf;
        }

        public static Weapon Sword(string name, int id)
        {
            Piece Blade = new Piece("Blade", GenerateMaterial.Steel, new Cylinder() { Height = 30, Radius = 2 }, true, true, false);

            Piece Hilt = new Piece("Hilt", GenerateMaterial.Maple, new Cylinder() { Height = 8, Radius = 1 }, true, false, true);

            Weapon sword = new Weapon(name);
            sword.AttackType = AttackType.Slashing;
            sword.ID = id;
            sword.AttachedPieces.Add(Blade);
            sword.AttachedPieces.Add(Hilt);

            Hilt.EquipmentOwner = sword;
            Blade.EquipmentOwner = sword;

            return sword;
        }

        public static Weapon Mace(string name, int id)
        {
            
            Piece ball = new Piece("Mace", GenerateMaterial.Steel, new Sphere() { Radius = 5 }, true, true, false);

            Piece rod = new Piece("Shaft", GenerateMaterial.Maple, new Cylinder() { Height = 40, Radius = 2 }, false, false, true);

            Weapon mace = new Weapon(name);
            mace.AttackType = AttackType.Blunt;
            mace.ID = id;
            mace.AttachedPieces.Add(ball);
            mace.AttachedPieces.Add(rod);

            ball.EquipmentOwner = mace;
            rod.EquipmentOwner = mace;

            return mace;
        }

        public static Armor Helmet(string name)
        {
            Piece Helm = new Piece(name, GenerateMaterial.Steel, new Sphere() { Radius = 8 }, false, false);

            Armor helmet = new Armor(name);
            helmet.ArmorPiece = Helm;

            Helm.EquipmentOwner = helmet;

            return helmet;
        }
    }
}
