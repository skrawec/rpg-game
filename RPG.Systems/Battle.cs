﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Battle
    {
        //Battle Method enemies and allies attending and the location
        public static void Start(List<Creature> enemies, List<Creature> allies, Location location, Random random)
        {
            //Calculate Turn Order
            //Order by Combined Equipped Weight and Self divided by agility divided by 10? 
            List<TurnOrder> turnOrders = CalculateTurnOrder(enemies, allies);
            List<IAction> actions = new List<IAction>();
            string input = String.Empty;

            bool endCombat = false;
            BattleTracker battleTracker = new BattleTracker();

            //Continue until enemies or allies are removed -- Run?
            while (!endCombat) 
            {
                //Display Ongoing effects
                Console.Clear();
                DisplayBattle(enemies, allies, location);
                
                //DisplayOngoingEffects(turnTracker, turnOrders, random);
                RegenPools(turnOrders);

                turnOrders = CalculateTurnOrder(enemies.Where(enemy => !enemy.IsDead).ToList() , allies.Where(ally => !ally.IsDead).ToList());
                
                BattleEnd(enemies, allies);

                if (!BattleEnd(enemies, allies))
                {
                    //Go through turn order
                    for (int turnCounter = 1; turnCounter < turnOrders.Count + 1; turnCounter++)
                    {
                        //Display 
                        Console.Clear();
                        DisplayBattle(enemies, allies, location);

                        TurnOrder currentCombatant = turnOrders.First(order => order.Order == turnCounter);

                        DisplayTurnOrder(turnOrders, turnCounter, currentCombatant);

                        TurnTracker turnTracker = new TurnTracker();
                        turnTracker.TurnTaker = currentCombatant.Combatant;
                        battleTracker.Turns.Add(turnTracker);
                        turnTracker.TurnTaker.RunOnGoingEffects(turnTracker, random);

                        //Generate list of actions possible base on the currentCombatant
                        actions.Clear();
                        actions = ActionsFactory.GetActionsFor(currentCombatant.Combatant);

                        bool endturn = false;

                        while (!endturn)
                        {
                            if (currentCombatant.IsAlly && !currentCombatant.Combatant.IsDead)
                            {
                                #region Select Action
                                //Display who's turn and their actions
                                DisplayTurnAction(actions, currentCombatant);
                                //Read input
                                input = Console.ReadLine().ToUpper();

                                //Get selected action                                
                                int actionIndex;
                                IAction currentAction = null;
                                
                                if(int.TryParse(input, out actionIndex))
                                    currentAction = actions[actionIndex-1];

                                ClearOptions(actions.Count());
                                #endregion

                                bool subActionSelected = false;

                                if (currentAction != null)
                                {
                                    while (!subActionSelected)
                                    {
                                        #region Select SubAction
                                        //Display selected action's subactions
                                        DisplaySubActions(currentAction);
                                        //read input
                                        input = Console.ReadLine().ToUpper();
                                        //get seleted subaction                                        
                                        int subActionIndex;
                                        SubAction currentSubAction = null;

                                        if (int.TryParse(input, out subActionIndex))
                                        {
                                            if (CheckInRange(currentAction.SubActions.Count, subActionIndex))    
                                                currentSubAction = currentAction.SubActions[subActionIndex - 1];
                                        }


                                        ClearOptions(currentAction.SubActions.Count() + 1);
                                        #endregion

                                        if (currentSubAction != null)
                                        {
                                            //check if auto targets
                                            if (!currentSubAction.AutoTarget)
                                            {
                                                #region Select Target and Execute Action

                                                bool targetSelected = false;

                                                while (!targetSelected)
                                                {
                                                    int targetCount = 0;
                                                    IEnumerable<Creature> combatants;
                                                    //display target list
                                                    if (currentSubAction.DefaultTargetsEnemies)
                                                    {
                                                        targetCount = DisplayTargetList(enemies);
                                                        combatants = enemies.Where(creature => !creature.IsDead);
                                                    }
                                                    else
                                                    {
                                                        targetCount = DisplayTargetList(allies);
                                                        combatants = allies.Where(creature => !creature.IsDead);
                                                    }

                                                    //read input
                                                    input = Console.ReadLine().ToUpper();
                                                    //get selected target
                                                    int targetIndex;
                                                    Creature currentTarget = null;

                                                    if (int.TryParse(input, out targetIndex))
                                                    {
                                                        if (CheckInRange(combatants.Count(), targetIndex))
                                                            currentTarget = combatants.ToList()[targetIndex - 1];
                                                    }

                                                    ClearOptions(targetCount + 1);

                                                    if (currentTarget != null)
                                                    {
                                                        //set target
                                                        currentSubAction.Target = currentTarget;

                                                        if (currentSubAction.LimbTarget)
                                                        {
                                                            bool limbSelected = false;

                                                            while (!limbSelected)
                                                            {
                                                                //display target list
                                                                DisplayLimbTargetList(currentTarget);
                                                                //read input
                                                                input = Console.ReadLine().ToUpper();

                                                                //get selected target                                                                
                                                                int limbIndex;
                                                                Limb currentLimbTarget = null;

                                                                if (int.TryParse(input, out limbIndex))
                                                                {
                                                                    if (CheckInRange(currentTarget.GetAllLimbs().Count(), limbIndex))
                                                                        currentLimbTarget = currentTarget.GetAllLimbs()[limbIndex - 1];
                                                                }

                                                                ClearOptions(currentTarget.GetAllLimbs().Count() + 1);

                                                                if (currentLimbTarget != null)
                                                                {
                                                                    currentSubAction.TargetLimb = currentLimbTarget;
                                                                    //execute action
                                                                    //TODO: Write out actions
                                                                    currentSubAction.ExecuteAction(turnTracker, random);
                                                                    //write output and any key to skip
                                                                    Console.WriteLine();                                                                    
                                                                    Console.WriteLine(turnTracker.TextOutput(random));
                                                                    Console.ReadLine();
                                                                    //end turn
                                                                    endturn = true;
                                                                    //end subaction loop
                                                                    subActionSelected = true;
                                                                    //end target loop
                                                                    targetSelected = true;
                                                                    //end limb target loop
                                                                    limbSelected = true;
                                                                }
                                                                else if (CheckForBackInput(currentTarget.GetAllLimbs().Count(), limbIndex))
                                                                {
                                                                    limbSelected = true;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //execute action
                                                            //TODO: Write out actions
                                                            currentSubAction.ExecuteAction(turnTracker, random);
                                                            //write output and any key to skip
                                                            Console.WriteLine();
                                                            Console.WriteLine(turnTracker.TextOutput(random));
                                                            Console.ReadLine();
                                                            //end turn
                                                            endturn = true;
                                                            //end subaction loop
                                                            subActionSelected = true;
                                                            //end target loop
                                                            targetSelected = true;
                                                        }
                                                    }
                                                    else if (CheckForBackInput(combatants.Count(), targetIndex))
                                                    {
                                                        targetSelected = true;
                                                    }
                                                }

                                                #endregion
                                            }
                                            else
                                            {
                                                //execute action and write output
                                                currentSubAction.ExecuteAction(turnTracker, random);
                                                Console.WriteLine();
                                                Console.WriteLine(turnTracker.TextOutput(random));
                                                Console.ReadLine();
                                                subActionSelected = true;
                                                endturn = true;
                                            }
                                        }
                                        else if (CheckForBackInput(currentAction.SubActions.Count, subActionIndex))
                                        {
                                            subActionSelected = true;
                                        }
                                    }
                                }
                                else
                                {
                                    endturn = true;
                                }
                            }
                            else if (!currentCombatant.Combatant.IsDead)
                            {
                                #region TODO: AI Action Selection

                                //Current only picks a random skill from what they can do
                                if (actions.Count > 0)
                                {
                                    int randomNumber = random.Next(actions.Count);
                                    var selectedAction = actions[randomNumber];

                                    int randomSubAction = random.Next(0, selectedAction.SubActions.Count - 1);
                                    var selectedSubAction = selectedAction.SubActions[randomSubAction];
                                    
                                    if (selectedSubAction.AutoTarget)
                                    {
                                        selectedSubAction.ExecuteAction(turnTracker, random);
                                    }
                                    else
                                    {
                                        var aliveTargets = allies.Where(ally => !ally.IsDead).ToList();
                                        int randomTarget = random.Next(0, aliveTargets.Count());
                                        var selectedTarget = aliveTargets[randomTarget];

                                        selectedSubAction.Target = selectedTarget;
                                        selectedSubAction.ExecuteAction(turnTracker, random);
                                    }
                                    Console.WriteLine();                                    
                                    Console.WriteLine(turnTracker.TextOutput(random));
                                }
                                else
                                {
                                    Console.WriteLine("{0} does nothing.", currentCombatant.Combatant.Name);
                                }

                                Console.ReadLine();
                                endturn = true;

                                #endregion
                            }
                            else
                            {
                                endturn = true;
                            }
                        }

                        //END OF BATTLE CHECK
                        //see if all enemies are dead or player is dead

                        if (BattleEnd(enemies, allies))
                        {
                            endCombat = true;
                            endturn = true;
                            turnCounter = turnOrders.Count + 1;
                            break;
                        }

                        
                    }
                }
                else
                {
                    endCombat = true;
                }
            }

            #region Wining/Losing Conditions
            
            if (enemies.Where(enemy => !enemy.IsDead).Count() == 0)
            {
                //Calculate Rewards
                int experience = CalcExperience(enemies);
                List<Item> items = CalcItems(enemies);
                int money = CalcMoney(enemies);

                //Split Experience
                var splitExperience = experience / allies.Count();

                //Award Experience
                foreach(Creature ally in allies)
                {
                      ally.ExperiencePoints += splitExperience;
                }
                Player player = (Player)allies.First(ally => ally.GetType() == typeof(Player));
                player.Items.AddRange(items);
                player.Money += money;
                //Display Experience, Money, and Items
                Console.WriteLine("Experience: {0}\n Money: {1}\n Loot:", splitExperience, money);
                foreach(Item item in items)
                {
                      Console.WriteLine("{0}", item.WorldlyName); 
                }
                //Display Win Screen
                Console.WriteLine("\nYou Win!");
            }
            else
            {
                //Display Lose Screen - End Game
                
                
                Console.WriteLine("\nYou Lose!");
            }
            Console.ReadLine();

            #endregion
        }

        private static bool CheckForBackInput(int count, int subActionIndex)
        {
            return subActionIndex == count + 1;
        }

        private static bool CheckInRange(int count, int subActionIndex)
        {           
            return subActionIndex > 0 && subActionIndex <= count;
        }

        private static void RegenPools(List<TurnOrder> turnOrders)
        {
            foreach (var combatant in turnOrders.Select(x => x.Combatant))
            {
                combatant.RegenMoralePower();
                combatant.BodyStrainTurnEnd();
            }
        }



        private static bool BattleEnd(List<Creature> enemies, List<Creature> allies)
        {
            int enemyCount = enemies.Where(enemy => !enemy.IsDead).ToList().Count();
            int playerCount = allies.Where(ally => !ally.IsDead).Count(ally => ally.GetType() == typeof(Player));

            if (enemyCount == 0 || playerCount == 0)
            {
                return true;
            }
            return false;
        }

        private static void DisplayOngoingEffects(TurnTracker turnTracker, List<TurnOrder> turnOrders, Random random)
        {
            Console.WriteLine("Turn Start");
            Console.WriteLine();

            var combatants = turnOrders.Select(to => to.Combatant).Where(combatant => !combatant.IsDead);
            foreach (var combatant in combatants)
            {
                combatant.RunOnGoingEffects(turnTracker, random);

                //TODO display all ongoing effects using turn tracker

                //if (!String.IsNullOrEmpty(output))
                  //  Console.Write(output);
            }

            Console.ReadLine();
        }

        #region Console Display Methods


        private static void GenerateBattleArea(Location location)
        {
            Console.WriteLine("Area: {0}", location.ToString());
        }

        private static int DisplayTargetList(List<Creature> creatures)
        {
            int count = 1;

            foreach (var creature in creatures.Where(creature => !creature.IsDead))
            {
                Console.WriteLine("{0}- {1}", count++, creature.Name);
            }
            Console.WriteLine("{0}- Back", count);
            Console.Write("\nAction:  ");

            return creatures.Where(creature => !creature.IsDead).Count();
        }

        private static void DisplayLimbTargetList(Creature target)
        {
            int count = 1;

            foreach (var limb in target.GetAllLimbs())
            {
                Console.Write("{0}- {1}", count++, limb.WorldlyName);
                Console.CursorLeft = 17;
                Console.Write("{0}/{1}", limb.CurrentDurability, limb.MaxDurablity);
                Console.CursorLeft = 27;
                foreach (var effect in limb.Effects)
                {
                    Console.Write("{0} ", effect.Name);
                }
                Console.WriteLine();
            }
            Console.WriteLine("{0}- Back", count);
            Console.Write("\nAction:  ");
        }

        private static void DisplaySubActions(IAction currentAction)
        {
            int count = 1;

            foreach (var subAction in currentAction.SubActions)
            {
                Console.Write("{0}- {1}", count++, subAction.Name);
                Console.CursorLeft = 20;
                Console.Write("Cost: {0}", subAction.StrainCost);
                Console.WriteLine();
            }
            Console.WriteLine("{0}- Back", count);
            Console.Write("\nAction:  ");
        }

        private static void DisplayTurnAction(List<IAction> actions, TurnOrder currentCombatant)
        {
            if (currentCombatant.Combatant.IsDisabled)
            {
                Console.WriteLine("\n{0} is current disabled and cannot take actions!", currentCombatant.Combatant.Name);
            }
            else if (actions.Count != 0)
            {
                int count = 1;
                foreach (var action in actions)
                {
                    Console.WriteLine("{0}- {1}", count++, action.Name);
                }

                Console.Write("\nAction:  ");
            }
            else
            {
                Console.WriteLine("\n{0} has no actions to take!", currentCombatant.Combatant.Name);
            }
        }

        private static void DisplayTurnOrder(List<TurnOrder> turnOrders, int turnCounter, TurnOrder currentCombatant)
        {
            Console.WriteLine("Turn Order:");
            int count = 0;
            foreach (TurnOrder turnOrder in turnOrders)
            {
                string turn = count == turnCounter -1 ? "*" : " ";
                Console.WriteLine("{0} : " + turn + " {1} \tSpeed: {2}", turnOrder.Order, turnOrder.Combatant.Name, decimal.Round(turnOrder.Combatant.Speed, 2));
                count++;
                
            }
            Console.WriteLine();
            Console.WriteLine("{1}'s turn. (Type your action)", turnCounter, currentCombatant.Combatant.Name);
        }

        private static void ClearOptions(int optionCount)
        {
            Console.CursorLeft = 0;
            Console.CursorTop -= optionCount + 2;
            for (int i = 0; i < optionCount; i++)
            {
                Console.WriteLine("                                                             ");
            }
            Console.WriteLine("                                                             ");
            Console.WriteLine("                                                             ");
            Console.WriteLine("                                                             ");
            Console.CursorLeft = 0;
            Console.CursorTop -= optionCount + 3;
        }

        private static void DisplayBattle(List<Creature> enemies, List<Creature> allies, Location location)
        {
            Console.WriteLine("Battle Begins");
            GenerateBattleArea(location);
            Console.WriteLine();
            //Display Enemies
            foreach (Creature enemy in enemies)
            {
                if (enemy.IsDead)
                    Console.WriteLine("Enemy: {0} \t DEAD ", enemy.Name);
                else
                {
                    Console.Write("Enemy: {0} \t {1}/{2} HP", enemy.Name, enemy.TotalCurrentHitPoints, enemy.TotalMaxHitPoints);
                    Console.Write(" {0}/{1} S", enemy.CurrentBodyStrain, enemy.MaxBodyStrain);
                    if (enemy.TotalMaxArmorPoints != 0)
                        Console.Write(" {0}/{1} AP", enemy.TotalCurrentArmorPoints, enemy.TotalMaxArmorPoints);
                    Console.WriteLine();
                    
                }
            }
            Console.WriteLine();
            //Display Allies
            foreach (Creature ally in allies)
            {
                if (ally.IsDead)
                    Console.WriteLine("Ally: {0} \t DEAD", ally.Name);
                else
                {
                    Console.Write("Ally: {0} \t {1}/{2} HP", ally.Name, ally.TotalCurrentHitPoints, ally.TotalMaxHitPoints);
                    Console.Write(" {0}/{1} S", ally.CurrentBodyStrain, ally.MaxBodyStrain);
                    if (ally.TotalMaxArmorPoints != 0)
                        Console.Write(" {0}/{1} AP", ally.TotalCurrentArmorPoints, ally.TotalMaxArmorPoints);
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
        }

        #endregion

        private static int CalcMoney(List<Creature> enemies)
        {
            return 10;
        }

        private static List<Item> CalcItems(List<Creature> enemies)
        {
            return new List<Item>();
        }

        private static int CalcExperience(List<Creature> enemies)
        {
            return 150;
        }

        private static List<TurnOrder> CalculateTurnOrder(List<Creature> enemies, List<Creature> allies, bool recalculateSpeedTurns = true)
        {
            int order = 1;
            List<TurnOrder> turnOrders = new List<TurnOrder>();
            List<Creature> allCombatants = new List<Creature>();

            allCombatants.AddRange(enemies);
            allCombatants.AddRange(allies);

            if (recalculateSpeedTurns)
            {
                allCombatants = allCombatants.OrderByDescending(combatant => combatant.Speed).ToList();
            }

            foreach (Creature combatant in allCombatants)
            {
                bool isAlly = allies.Contains(combatant);

                turnOrders.Add(new TurnOrder(combatant, order++, isAlly));
            }

            return turnOrders;
        }


    }

    public class TurnOrder
    {
        public TurnOrder(Creature combatant, int order, bool isAlly)
        {
            Combatant = combatant;
            Order = order;
            IsAlly = isAlly;
        }

        public Creature Combatant { get; private set; }
        public int Order { get; set; }
        public bool IsAlly { get; private set; }
    }
}
