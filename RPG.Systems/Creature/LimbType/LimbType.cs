﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public abstract class LimbType
    {
        public LimbType()
        {
            DestroyedWords = new List<string>();
        }

        private const int DAMAGED_BLEED_DAMAGE = 1;
        private const int DESTORYED_BLEED_DAMAGE = 2;
        //internal Limb _limb;
        public List<string> DestroyedWords { get; }

        public virtual void IsHit(ObjectStateTracker objectDamageTracker, IAttack attack, Random random)
        {
            Limb limb = objectDamageTracker.TargetObject as Limb;

            //get the incoming weapon damage
            decimal incomingWeaponDamage = attack.CalcDamage(random);

            decimal totalResistance = 0;

            //if attack is resistable
            if (attack.IsResistable)
            {
                totalResistance = limb.CalcTotalResistance(attack);
            }

            //calculate potential damage
            int totalDamage = limb.CalcTotalDamage(incomingWeaponDamage, totalResistance);

            int totalResisted = (int)Math.Round(incomingWeaponDamage) - totalDamage;

            //setup the limb tracker
            objectDamageTracker.Damage = totalDamage;
            objectDamageTracker.AttackType = attack.AttackType;
            objectDamageTracker.ResistedDamage = totalResisted;

            //finally reduce durablity by the rounded total damage
            if (limb.CurrentDurability < totalDamage)
            {
                totalDamage -= limb.CurrentDurability;
                limb.CurrentDurability = 0;
            }
            else
            {
                limb.CurrentDurability -= totalDamage;
            }
        }

        public virtual void IsDamaged(TargetTracker targetTracker, ObjectStateTracker objectStateTracker, IAttack attack)
        {
            Limb limb = objectStateTracker.TargetObject as Limb;

            limb.IsDamaged = true;
            if ((attack.AttackType == AttackType.Blunt ||
                attack.AttackType == AttackType.Piercing ||
                attack.AttackType == AttackType.Slashing &&
                limb.Material.IsOrganic))
            {
                var bleeding = new Bleeding(limb, DAMAGED_BLEED_DAMAGE);
                bleeding.Apply(targetTracker, limb);
            }
        }

        public virtual void IsBroken(TargetTracker targetTracker, ObjectStateTracker objectDamageTracker, IAttack attack)
        {
            Limb targetLimb = objectDamageTracker.TargetObject as Limb;

            targetLimb.IsDamaged = true;
            targetLimb.AttachedLimbs.ForEach(limb => limb.DisableLimb());

            //drop weapon if this limb is equipped with one
            if (targetLimb.WeaponEquipped != null)
            {
                targetTracker.EquipmentDropped.Add(targetLimb.WeaponEquipped);
                targetLimb.DeEquipWeapon();
            }

            objectDamageTracker.IsDamaged = true;
        }

        public virtual void IsDestroyed(TargetTracker targetTracker, ObjectStateTracker objectDamageTracker, IAttack attack, Random random)
        {
            Limb targetLimb = objectDamageTracker.TargetObject as Limb;

            //count reduced max durablility
            targetTracker.ReducedLimbMaxDurability = targetLimb.ParentLimb.AttachedLimbs.First(limb => limb == targetLimb).GetMaxHp();

            targetTracker.ObjectDropped.Add(targetLimb);

            //destroy limb
            targetLimb.ParentLimb.AttachedLimbs.Remove(targetLimb);
            targetLimb.ParentLimb.IsSevered = true;

            //Add bleeding effect to severed limb
            var bleeding = new Bleeding(targetLimb.ParentLimb, DESTORYED_BLEED_DAMAGE);
            bleeding.Apply(targetTracker, targetLimb.ParentLimb);

            targetLimb.ParentLimb = null;

            //drop weapons
            var limbWeapons = targetLimb.AttachedLimbs.Select(limb => limb.GetAllWeapons(true));
            foreach (var weapons in limbWeapons)
            {
                weapons.ForEach(weapon => targetTracker.EquipmentDropped.Add(weapon));
            }
            //get armor dropped
            var limbArmors = targetLimb.AttachedLimbs.Select(limb => limb.GetAllArmors());
            foreach (var armors in limbArmors)
            {
                foreach (var armor in armors)
                {
                    targetTracker.ReducedArmorMaxDurability += armor.ArmorPiece.MaxDurablity;
                }
            }

            if (targetLimb.WeaponEquipped != null)
            {
                targetTracker.EquipmentDropped.Add(targetLimb.WeaponEquipped);
            }

            objectDamageTracker.IsDestroyed = true;

            int randomDestroyedWord = random.Next(DestroyedWords.Count);
        }

        public virtual void IsDisabled(ObjectStateTracker objectDamageTracker, IAttack attack)
        {
            (objectDamageTracker.TargetObject as Limb).DisableLimb();
        }
    }
}
