﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class UpperBody : LimbType
    {
        public UpperBody(Limb limb)
        {
            DestroyedWords.AddRange(limb.Material.DestroyedWords);
        }
    }
}
