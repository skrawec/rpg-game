﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Leg : LimbType
    {
        public Leg(Limb limb)
        {
            limb.IsUsedForMobility = true;            
            DestroyedWords.AddRange(limb.Material.DestroyedWords);
        }

        public override void IsDamaged(TargetTracker targetTracker, ObjectStateTracker objectStateTracker, IAttack attack)
        {
            base.IsDamaged(targetTracker, objectStateTracker, attack);

            Limb limb = objectStateTracker.TargetObject as Limb;

            if (limb.IsUsedForMobility)
            {
                limb.LimbOwner.GetCurrentCountMobilityLimbs();   
            }
        }
    }
}
