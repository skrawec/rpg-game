﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    class Misc : LimbType
    {
        public Misc(Limb limb)
        {
            DestroyedWords.AddRange(limb.Material.DestroyedWords);
        }
    }
}
