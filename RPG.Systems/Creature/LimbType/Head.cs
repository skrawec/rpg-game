﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Head : LimbType
    {
        private const int STUNNED_TURN_LENGTH = 1;

        public Head(Limb limb)
        {
            //_limb = limb;
            DestroyedWords.AddRange(limb.Material.DestroyedWords);
            DestroyedWords.Add("decapitated");
            //TODO GetRandomDestroyed word override from object and PIECE and LIMB have to overright with their versionss
        }

        public override void IsDamaged(TargetTracker targetTracker, ObjectStateTracker objectStateTracker, IAttack attack)
        {
            base.IsDamaged(targetTracker, objectStateTracker, attack);
            Limb limb = objectStateTracker.TargetObject as Limb;

            if (limb.IsDamaged)
            {
                Stunned stunned = new Stunned(limb.LimbOwner, STUNNED_TURN_LENGTH);
                stunned.Apply(targetTracker);
            }
        }
    }
}
