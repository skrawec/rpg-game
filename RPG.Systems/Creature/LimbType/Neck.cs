﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Neck : LimbType
    {
        public Neck(Limb limb)
        {
            DestroyedWords.AddRange(limb.Material.DestroyedWords);
            DestroyedWords.Add("Decapitated");
        }
    }
}
