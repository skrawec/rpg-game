﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public enum Condition
    {
        Tired,
        Thristy,
        Hungry,
        MissingEye,
        Blind,
        //etc...
    }
}
