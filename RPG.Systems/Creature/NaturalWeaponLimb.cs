﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class NaturalWeaponLimb : Limb, IAttack
    {        
        public NaturalWeaponLimb(
            string name, 
            Creature limbOwner, 
            bool isVital, 
            Material material, 
            I3DShape shape, 
            AttackType attackType,
            bool isResistable,
            Side? side = default(Side?), 
            int? curDur = default(int?)) 
            : base(name, limbOwner, isVital, material, shape, side, curDur)
        {
            AttackType = attackType;
            IsResistable = isResistable;
        }

        public AttackType AttackType { get; set; }
        public bool IsResistable { get; private set; }

        public decimal CalcDamage(Random random)
        {
            return CombatHelper.CalcDamage(random, this, LimbOwner);
        }

        public decimal GetResistValue()
        {
            return CombatHelper.CalcResist(this);
        }
    }
}
