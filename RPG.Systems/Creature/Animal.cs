﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Animal : Creature
    {
        public Animal(string name, int str, int intel, int agi, int wis, int con, int cha) : base(name, str, intel, agi, wis, con, cha)
        {
        }
    }
}
