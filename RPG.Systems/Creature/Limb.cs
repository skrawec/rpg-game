﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public class Limb : Object, IEquipable
    {
        public Limb(string name, Creature limbOwner, bool isVital, Material material, I3DShape shape, Side? side = null, int? curDur = null) : base(name, material, shape, true, curDur)
        {
            AttachedLimbs = new List<Limb>();
            Effects = new List<IEffect>();
            IsVital = isVital;
            LimbOwner = limbOwner;
            Side = side;
        }

        public Side? Side { get; set; }
        public bool IsBroken { get; set; }
        public bool IsDisabled { get; set; }
        public Creature LimbOwner { get; set; }
        public bool IsUsedForMobility { get; set; }
        public bool IsWeaponMountable { get; set; }
        public bool IsVital { get; set; }
        public Limb ParentLimb { get; set; }
        public List<IEffect> Effects { get; set; }
        public List<Limb> AttachedLimbs { get; set; }
        public Armor ArmorEquipped { get; private set; }
        public Weapon WeaponEquipped { get; private set; }
        public bool IsSevered { get; set; }
        public LimbType LimbType { get; set; }
        public string Name
        {
            get { return string.Format("{0}{1}{2}", Side, Side == null ? "" : " ", WorldlyName); }
        }

        public void Equip(Weapon weapon)
        {
            WeaponEquipped = weapon;
        }

        public void Equip(Armor armor)
        {
            ArmorEquipped = armor;
        }

        public void DeEquipArmor()
        {
            if (ArmorEquipped != null)
            {
                ArmorEquipped.LimbEquippedOn = null;
                ArmorEquipped.Weilder = null;
                ArmorEquipped = null;
            }
        }

        public void DeEquipWeapon()
        {
            if (WeaponEquipped != null)
            {
                WeaponEquipped.LimbEquippedOn = null;
                WeaponEquipped.Weilder = null;
                WeaponEquipped = null;
            }
        }

        public Weapon TryEquip(Weapon weapon)
        {
            Weapon weaponToRemove = WeaponEquipped;

            if (WeaponEquipped != null)
            {
                weapon.LimbEquippedOn = this;
                weapon.Weilder = weaponToRemove.Weilder;
                weaponToRemove.LimbEquippedOn = null;
                weaponToRemove.Weilder = null;
                WeaponEquipped = weapon;
            }
            else
            {
                Equip(weapon);
            }

            return weaponToRemove;
        }

        public Armor TryEquip(Armor armor)
        {
            Armor armorToRemove = ArmorEquipped;
            if (ArmorEquipped != null)
            {
                armor.LimbEquippedOn = this;
                armor.Weilder = armorToRemove.Weilder;
                armorToRemove.Weilder = null;
                armorToRemove.LimbEquippedOn = null;
                ArmorEquipped = armor;
            }
            else
            {
                Equip(armor);
            }
            return armorToRemove;
        }

        public Equipment GetEquip()
        {
            return ArmorEquipped;
        }

        public List<Equipment> GetAllEquipment()
        {
            List<Equipment> equipments = new List<Equipment>();

            if (ArmorEquipped != null)
                equipments.Add(ArmorEquipped);
            if (WeaponEquipped != null)
                equipments.Add(WeaponEquipped);

            foreach (var limbEquipment in AttachedLimbs.Select(limb => limb.GetAllEquipment()))
            {
                equipments.AddRange(limbEquipment);
            }

            return equipments;
        }

        public List<Armor> GetAllArmors()
        {
            List<Armor> armors = new List<Armor>();

            if (ArmorEquipped != null)
            {
                armors.Add(ArmorEquipped);
            }

            foreach (var limbEquipment in AttachedLimbs.Select(limb => limb.GetAllArmors()))
            {
                armors.AddRange(limbEquipment);
            }

            return armors;
        }

        public List<Weapon> GetAllWeapons(bool includeDisabledLimbs)
        {
            List<Weapon> weapons = new List<Weapon>();

            if (WeaponEquipped != null)
            {
                weapons.Add(WeaponEquipped);
            }

            var limbs = AttachedLimbs;

            if (!includeDisabledLimbs)
            {
                limbs = limbs.Where(limb => !limb.IsDisabled).ToList();
            }

            foreach (var limbEquipment in limbs.Select(limb => limb.GetAllWeapons(includeDisabledLimbs)))
            {
                weapons.AddRange(limbEquipment);
            }

            return weapons;
        }

        internal List<NaturalWeaponLimb> GetNaturalWeapons()
        {
            List<NaturalWeaponLimb> limbWeapons = new List<NaturalWeaponLimb>();

            foreach (var limb in AttachedLimbs)
            {
                limbWeapons.AddRange(limb.GetNaturalWeapons());
            }

            if (this is NaturalWeaponLimb)
            {
                limbWeapons.Add(this as NaturalWeaponLimb);
            }

            return limbWeapons;
        }

        internal decimal GetBodyWeight()
        {
            return Weight + AttachedLimbs.Sum(limb => limb.GetBodyWeight());
        }

        internal decimal GetEquippedWeight()
        {
            return AttachedLimbs.Sum(limb => limb.GetEquippedWeight()) + (ArmorEquipped == null ? 0 : ArmorEquipped.ArmorPiece.Weight);
        }

        internal int GetCurrentHp()
        {
            return CurrentDurability + AttachedLimbs.Sum(limb => limb.GetCurrentHp());
        }

        internal int GetMaxHp()
        {
            return MaxDurablity + AttachedLimbs.Sum(limb => limb.GetMaxHp());
        }

        internal int GetMaxArmorPoints()
        {
            return (ArmorEquipped != null ? ArmorEquipped.ArmorPiece.MaxDurablity : 0) + AttachedLimbs.Sum(limb => limb.GetMaxArmorPoints());
        }

        

        internal int GetCurrentArmorPoints()
        {
            return (ArmorEquipped != null ? ArmorEquipped.ArmorPiece.CurrentDurability : 0) + AttachedLimbs.Sum(limb => limb.GetCurrentArmorPoints());
        }

        /// <summary>
        /// Get the count of limbs used for moving
        /// </summary>
        /// <returns>A interger representing the count</returns>
        internal int GetCountMobilityLimbs()
        {
            int mobileLimbCount = 0;

            if (IsUsedForMobility && !IsDamaged)
                mobileLimbCount++;

            return mobileLimbCount + AttachedLimbs.Sum(limb => limb.GetCountMobilityLimbs());
        }

        /// <summary>
        /// Get this and attached limbs
        /// </summary>
        /// <returns>A Collection of limbs</returns>
        internal List<Limb> GetAttachedLimbs()
        {
            List<Limb> allLimbs = new List<Limb>();

            allLimbs.Add(this);

            foreach (var limbs in AttachedLimbs.Select(limb => limb.GetAttachedLimbs()))
            {
                allLimbs.AddRange(limbs);
            }

            return allLimbs;
        }

        /// <summary>
        /// override Take Damage on the limb
        /// </summary>
        /// <param name="random">Single random class</param>
        /// <param name="weapon">Incoming Weapon to use in calculations</param>
        /// <returns>A DamageDescription class that houses everything that happened</returns>
        public override void Damage(TurnTracker turnTracker, IAttack attack, Random random)
        {
            //Check if limb has armor equipped, if not hit limb, else hit armor
            if (ArmorEquipped == null || ArmorEquipped.ArmorPiece.IsDestroyed)
            {
                //Setup trackers            
                ObjectStateTracker objectStateTracker = new ObjectStateTracker(this);
                objectStateTracker.DoesDamage = true;

                LimbType.IsHit(objectStateTracker, attack, random);

                TargetTracker targetTracker = new TargetTracker(LimbOwner, objectStateTracker);

                turnTracker.Targets.Add(targetTracker);


                //Limb is severed if below destroyed point
                if (CurrentDurability == 0)// && totalDamage * (-1) < DestroyedPoint)
                {
                    if (ParentLimb != null && attack.AttackType ==AttackType.Slashing)
                    {
                        LimbType.IsDestroyed(targetTracker, objectStateTracker, attack, random);
                    }
                    //Drop weapon and break limb if blunt
                    else if (attack.AttackType == AttackType.Blunt)
                    {
                        LimbType.IsBroken(targetTracker, objectStateTracker, attack);
                    }
                    else
                    {
                        LimbType.IsDisabled(objectStateTracker, attack);
                    }

                    //Kill creature if vital limb
                    if (IsVital == true)
                    {
                        LimbOwner.IsDead = true;
                        turnTracker.DeadCreatures.Add(LimbOwner);
                    }

                }
                else if ((MaxDurablity / 2) > CurrentDurability && !IsDamaged)
                {
                    LimbType.IsDamaged(targetTracker, objectStateTracker, attack);
                }
            }
            else
            {
                ArmorEquipped.ArmorPiece.Damage(turnTracker, attack, random);
            }
        }

        /// <summary>
        /// Disable this and attached limbs
        /// </summary>
        public void DisableLimb()
        {
            IsDisabled = true;
            AttachedLimbs.ForEach(limb => limb.DisableLimb());
        }

        private void BreakLimb()
        {
            IsDamaged = true;
            DisableLimb();
        }

        /// <summary>
        /// Get this and attached limbs mountable limbs (limbs with flag IsWeaponMountable)
        /// </summary>
        /// <returns>A collection of weapon mountable limbs</returns>
        internal List<Limb> GetWeaponMountableLimbs()
        {
            List<Limb> mountableLimbs = new List<Limb>();

            if (IsWeaponMountable)
                mountableLimbs.Add(this);

            foreach (var limbs in AttachedLimbs.Select(limb => limb.GetWeaponMountableLimbs()))
            {
                mountableLimbs.AddRange(limbs);
            }

            return mountableLimbs;
        }


        /// <summary>
        /// Get all the effects on this and it's attached limbs
        /// </summary>
        /// <returns>A Collection of effects gathered</returns>
        internal List<IEffect> GetEffects()
        {
            List<IEffect> effects = new List<IEffect>();

            effects.AddRange(Effects);

            foreach (var effectsFromLimbs in AttachedLimbs.Select(limb => limb.GetEffects()))
            {
                effects.AddRange(effectsFromLimbs);
            }

            return effects;
        }

        /// <summary>
        /// Run ongoing effects on this limb and then calls it's attached limbs ongoing effects
        /// </summary>
        /// <param name="random">single random class</param>
        /// <returns>This and attached limbs string output</returns>
        public void RunOnGoingLimbEffects(TurnTracker turnTracker, Random random)
        {            
            //Get a list of effects to remove
            //Effects.Where(effect => effect.ToRemove)
            //       .ToList()
            //       .ForEach(effect => effect.ExitEffect(objectDamageTracker));

            //Effects.RemoveAll(effect => effect.ToRemove);

            if (AttachedLimbs.Count != 0)
            {
                for (int i = 0; i < AttachedLimbs.Count(); i++)
                {
                    AttachedLimbs[i].RunOnGoingLimbEffects(turnTracker, random);
                }
            }

            for (int i = 0; i < Effects.Count; i++)
            {
                Effects[i].OnGoing(turnTracker, random);
            }

            ////Get a list of effects to remove
            //Effects.Where(effect => effect.ToRemove)
            //       .ToList()
            //       .ForEach(effect => effect.ExitEffect(objectDamageTracker));

            //Effects.RemoveAll(effect => effect.ToRemove);            
        }

        public void TryResistOnGoingEffects(Random random)
        {            
            if (AttachedLimbs.Count != 0)
            {
                for (int i = 0; i < AttachedLimbs.Count(); i++)
                {
                    AttachedLimbs[i].TryResistOnGoingEffects(random);
                }
            }

            foreach (IResistableEffect effect in Effects.Where(x => x is IResistableEffect))
            {
                effect.TryResist(random);
            }
        }

        public void TrySpreadOnGoingEffects(TurnTracker turnTracker, Random random)
        {            
            if (AttachedLimbs.Count != 0)
            {
                for (int i = 0; i < AttachedLimbs.Count(); i++)
                {
                    AttachedLimbs[i].TrySpreadOnGoingEffects(turnTracker, random);
                }
            }

            foreach (ISpreadableEffect effect in Effects.Where(x => x is ISpreadableEffect))
            {
                effect.TrySpreading(turnTracker, random);
            }
        }

        /// <summary>
        /// Finalize spread effects
        /// </summary>
        public void SpreadFinalize()
        {
            foreach (Limb limb in AttachedLimbs)
            {
                limb.SpreadFinalize();
            }

            foreach (ISpreadableEffect effect in Effects.Where(x => x is ISpreadableEffect))
            {
                effect.HasJustSpread = false;
            }


        }

        internal List<Limb> GetAdjacentLimbs()
        {
            List<Limb> adjacentLimbs = new List<Limb>();

            //Add adjacent limbs to equation
            adjacentLimbs.AddRange(AttachedLimbs);
            if (ParentLimb != null)
            {
                adjacentLimbs.Add(ParentLimb);
            }

            return adjacentLimbs;
        }

        public void AttachLimb(Limb limb)
        {
            AttachedLimbs.Add(limb);
            limb.ParentLimb = this;
        }
    }
}
