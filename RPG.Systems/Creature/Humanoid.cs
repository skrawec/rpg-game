﻿namespace RPG.Systems
{
    public class Humanoid : Creature
    {
        public Humanoid(string name, int str, int intel, int agi, int wis, int con, int cha) : base(name, str, intel, agi, wis, con, cha)
        {
        }
    }
}
