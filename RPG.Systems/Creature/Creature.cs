﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public abstract class Creature : WorldObject
    {
        public Creature(string name, int str, int intel, int agi, int wis, int con, int cha) : base(name)
        {
            Limbs = new List<Limb>();
            Effects = new List<ICreatureEffect>();
            Items = new List<Item>();
            Manifestations = new List<IManifestation>();
            Strength = str;
            Intelligence = intel;
            Agility = agi;
            Wisdom = wis;
            Constitution = con;
            Charisma = cha;
            CurrentBodyStrain = 0;
        }

        private int _maxMobilityLimbs;

        public int MaxMobilityLimbs 
        { 
            get 
            {
                if(_maxMobilityLimbs == 0)
                    _maxMobilityLimbs = GetCurrentCountMobilityLimbs();

                return _maxMobilityLimbs;
            } 
        }

        public int MaxMorale
        {
            get
            {
                return 100;
            }
        }

        public int CurrentMorale { get; set; }

        public int MaxBodyStrain
        {
            get
            {
                return (int)decimal.Round(20M * ((decimal)Wisdom / 10M));
            }
        }

        public int CurrentBodyStrain { get; set; }

        public Gender Gender { get; set; }
        public List<IManifestation> Manifestations { get; set; }
        public List<Item> Items { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsDead { get; set; }
        public List<Limb> Limbs { get; set; }
        public List<Condition> Conditions { get; set; }
        public List<ICreatureEffect> Effects { get; set; }
        public int ExperiencePoints { get; set; }

        public int Hunger { get; set; }
        public int Thrist { get; set; }

        public int Dirty { get; set; }
        public int Stamina { get; set; }
        public int Tiredness { get; set; }
        public int Strength { get; set; }
        public int Intelligence { get; set; }
        public int Agility { get; set; }
        public int Wisdom { get; set; }
        public int Charisma { get; set; }
        public int Constitution { get; set; }

        private string _name;

        public string Name { get { return _name == null ? WorldlyName : _name; } set { _name = value; } }

        public int TotalMaxHitPoints 
        {
            get { return Limbs.Sum(limb => limb.GetMaxHp()); }
        }
        public int TotalCurrentHitPoints 
        {
            get { return Limbs.Sum(limb => limb.GetCurrentHp()); }
        }

        public int TotalMaxArmorPoints
        {
            get { return Limbs.Sum(limb => limb.GetMaxArmorPoints()); }
        }

        public int TotalCurrentArmorPoints
        {
            get { return Limbs.Sum(limb => limb.GetCurrentArmorPoints()); }
        }

        public decimal CarryWeight
        {
            get { return Strength / 10M * 30000; }
        }

        public decimal Speed
        {
            get { return Agility * 10M / (GetPersonalWeight() + GetOverweightPenalty()) * 1000 * (GetCurrentCountMobilityLimbs() / (decimal)MaxMobilityLimbs); }
        }

        public int GetCurrentCountMobilityLimbs()
        {
            return Limbs.Sum(limb => limb.GetCountMobilityLimbs());
        }

        /// <summary>
        /// Get list of current effects
        /// </summary>
        /// <returns></returns>
        public List<IEffect> GetEffects()
        { 
            List<IEffect> effects = new List<IEffect>();

            foreach (IEnumerable<IEffect> effectsFromLimbs in Limbs.Select(limb => limb.GetEffects()))
            {
                effects.AddRange(effectsFromLimbs);
            }

            return effects;
        }

        public Dictionary<Limb, List<IEffect>> GetEffectsByLimb()
        {
            Dictionary<Limb, List<IEffect>> effectsByLimb = new Dictionary<Limb, List<IEffect>>();

            foreach (Limb limb in Limbs.Select(limb => limb))
            {
                effectsByLimb.Add(limb, limb.GetEffects());
            }

            return effectsByLimb;
        }
        
        /// <summary>
        /// Resist the effect/shrug it off
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        internal bool Resist(IResistableEffect effect, Random random)
        {
            bool resist = false;

            int inheritReistBonus = 4;

            int constitutionModifier = Constitution < 10 ? 0 : Constitution - 10;

            int resistStrength = constitutionModifier * inheritReistBonus; //Add bonuses (skills)

            int chanceToResist = Math.Abs(resistStrength / (int)effect.EffectSeverity);

            int roll = random.Next(1, 100);

            if (roll < chanceToResist)
                resist = true;

            return resist;
        }

        /// <summary>
        /// Run all ongoing effects on creature        
        /// </summary>
        /// <param name="random"></param>
        /// <returns></returns>
        public void RunOnGoingEffects(TurnTracker turnTracker, Random random)
        {           
            Limbs.ForEach(limb => limb.TryResistOnGoingEffects(random));
            Limbs.ForEach(limb => limb.RunOnGoingLimbEffects(turnTracker, random));
            Limbs.ForEach(limb => limb.TrySpreadOnGoingEffects(turnTracker, random));
            Limbs.ForEach(limb => limb.SpreadFinalize());
            
            foreach (var effect in Effects)
            {
                effect.OnGoing(turnTracker, random);
            }

            //Get a list of effects to remove
            Effects.Where(effect => effect.ToRemove)
                   .ToList()
                   .ForEach(effect => effect.ExitEffect(turnTracker));

            Effects.RemoveAll(effect => effect.ToRemove);            
        }

        /// <summary>
        /// Regen Body Strain each tick/turn
        /// </summary>
        /// <returns>Amount Regenerated</returns>
        public int BodyStrainTurnEnd()
        {
            decimal calcRegen = MaxBodyStrain * 0.1M;

            int regen = (int)decimal.Round(calcRegen, 0);

            if (regen == 0)
            {
                regen = 1;
            }
            else if (regen > MaxBodyStrain - CurrentBodyStrain)
            {
                regen = MaxBodyStrain - CurrentBodyStrain;
            }

            CurrentBodyStrain += regen;
            return regen;

        }

        /// <summary>
        /// Regen Morale Power each tick/turn
        /// </summary>
        /// <returns>Amount Regenerated</returns>
        public int RegenMoralePower()
        {
            int regen = (int)decimal.Round(MaxMorale * 0.1M, 0);

            if (regen == 0)
            {
                regen = 1;
            }
            else if (regen > MaxMorale - CurrentMorale)
            {
                regen = MaxMorale - CurrentMorale;
            }

            CurrentBodyStrain += regen;

            return regen;
        }

        /// <summary>
        /// Get Weapon Mountable limbs
        /// </summary>
        /// <returns>List of Limbs</returns>
        public List<Limb> GetWeaponMountableLimbs()
        { 
            List<Limb> mountableLimbs = new List<Limb>();

            foreach (var limbs in Limbs.Select(limb => limb.GetWeaponMountableLimbs()))
	        {
                mountableLimbs.AddRange(limbs);
	        }

            return mountableLimbs;
        }

        /// <summary>
        /// Equip Weapon to Mount
        /// </summary>
        /// <param name="selectedLimb"></param>
        /// <param name="weapon"></param>
        public void EquipWeapon(Limb selectedLimb, Weapon weapon)
        {
             Limb limbToEquip = GetAllLimbs().FirstOrDefault(limb => limb == selectedLimb);

            if(limbToEquip == null)
                throw new Exception(string.Format("Limb: {0} does not exist on Creature: {1}", selectedLimb.WorldlyName, WorldlyName));

            if (!limbToEquip.IsWeaponMountable)
                throw new Exception(string.Format("Limb: {0} does not have a mountable weapon slot", selectedLimb.WorldlyName));
            
            limbToEquip.TryEquip(weapon);

            weapon.Weilder = this;
            weapon.LimbEquippedOn = limbToEquip;
        }

        /// <summary>
        /// Equip Armor
        /// </summary>
        /// <param name="selectedLimb">Limb to equip</param>
        /// <param name="armor">Armor to equip</param>
        public void EquipArmor(Limb selectedLimb, Armor armor)
        {
            Limb limbToEquip = GetAllLimbs().FirstOrDefault(limb => limb == selectedLimb);

            if (limbToEquip == null)
                throw new Exception(string.Format("Limb: {0} does not exist on Creature: {1}", selectedLimb.WorldlyName, WorldlyName));
            
            limbToEquip.TryEquip(armor);

            armor.Weilder = this;
            armor.LimbEquippedOn = limbToEquip;
        }

        /// <summary>
        /// Get all equipped weapons
        /// </summary>
        /// <param name="includeDisabledLimbs">Include disabled limbs?</param>
        /// <returns>List of Weapons</returns>
        public List<Weapon> GetEquippedWeapons(bool includeDisabledLimbs = false)
        {
            List<Weapon> weapons = new List<Weapon>();

            var limbs = Limbs;
            
            if(!includeDisabledLimbs)
            {
                limbs = limbs.Where(limb => !limb.IsDisabled).ToList();
            }

            foreach (var limbEquipment in limbs.Select(limb => limb.GetAllWeapons(includeDisabledLimbs)))
            {
                weapons.AddRange(limbEquipment);
            }            

            return weapons;
        }
        /// <summary>
        /// Get useable weapons
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, IAttack> GetUseableWeapons()
        {
            Dictionary<string, IAttack> attacks = new Dictionary<string, IAttack>();

            IEnumerable<Limb> limbs = Limbs.Where(limb => !limb.IsDisabled);

            IEnumerable<Weapon> useableWeapons = Limbs.SelectMany(limb => limb.GetAllWeapons(false));
            IEnumerable<NaturalWeaponLimb> useableLimbWeapons = Limbs.SelectMany(limb => limb.GetNaturalWeapons());

            foreach (Weapon useableWeapon in useableWeapons)
            {
                attacks.Add(useableWeapon.Name, useableWeapon);
            }

            foreach (NaturalWeaponLimb useableNaturalWeapon in useableLimbWeapons)
            {
                attacks.Add(useableNaturalWeapon.Name, useableNaturalWeapon);
            }
            
            return attacks;
        }

        /// <summary>
        /// Get all currently equipped armor
        /// </summary>
        /// <returns>List of armor</returns>
        public List<Armor> GetEquippedArmor()
        {
            List<Armor> armor = new List<Armor>();
            List<Equipment> equipments = new List<Equipment>();

            foreach (var limbEquipment in Limbs.Select(limb => limb.GetAllEquipment()))
            {
                equipments.AddRange(limbEquipment);
            }

            if (equipments.Count != 0)
            {
                foreach (var weapon in equipments.Where(equip => equip is Armor))
                {
                    armor.Add((Armor)weapon);
                }
            }

            return armor;
        }
        
        /// <summary>
        /// If currently equipped weight is over carry weight add on the addtional weight to speed calculation
        /// </summary>
        /// <returns></returns>
        internal decimal GetOverweightPenalty()
        {
            decimal penalty = GetEquippedWeight() - CarryWeight;

            if (penalty > 0)
                return penalty;

            return 0;
        }

        internal decimal GetPersonalWeight()
        {
            return Limbs.Sum(limb => limb.GetBodyWeight());
        }

        internal decimal GetEquippedWeight()
        {
            return Limbs.Sum(limb => limb.GetEquippedWeight());
        }

        internal List<Limb>GetAllLimbs()
        {
            List<Limb> allLimbs = new List<Limb>();

            foreach (var limbs in Limbs.Select(limb => limb.GetAttachedLimbs()))
            {
                allLimbs.AddRange(limbs);
            }

            return allLimbs;
        }

        internal List<Limb> GetLimbsOfType(Type type)
        {
            return GetAllLimbs().Where(limb => limb.LimbType.GetType() == type).ToList();
        }

    }
}
