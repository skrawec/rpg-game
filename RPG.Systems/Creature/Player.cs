﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class Player : Creature
    {
        public Player(string name, int str, int intel, int agi, int wis, int con, int cha) : base (name, str, intel, agi, wis, con, cha)
        {
            Skills = new List<Skill>();
            Items = new List<Item>();
        }

        public decimal Money { get; set; }

        public string GivenName { get; set; }
        public string NickName { get; set; }

        public List<Skill> Skills { get; set; }
        
        
        public Race Race { get; set; }
    }
}
