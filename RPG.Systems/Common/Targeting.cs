﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public static class Targeting
    {
        public static Object RandomLimb(Creature target, Random random)
        {            
            List<Object> allTargets = new List<Object>();
            List<TargetChance> targetChances = new List<TargetChance>();

            allTargets.AddRange(target.GetAllLimbs());
            allTargets.AddRange(target.GetEquippedWeapons().SelectMany(x => x.AttachedPieces));

            decimal totalVolume = allTargets.Sum(x => x.Volume);

            int startAmount = 0;

            foreach (Object possibleTarget in allTargets)
            {
                startAmount++;

                int currentAmount = (int)Math.Abs(possibleTarget.Volume / totalVolume * 1000) + startAmount;
                
                targetChances.Add(new TargetChance(possibleTarget, startAmount, currentAmount));

                startAmount = currentAmount;
            }


            int randomNumber = Math.Abs(random.Next(targetChances.First().MinRange, targetChances.Last().MaxRange));

            Object selectedTarget = targetChances.FirstOrDefault(x => x.MinRange <= randomNumber && x.MaxRange >= randomNumber).Target;

            return selectedTarget;
        }


    }

    public class TargetChance
    {
        public TargetChance(Object target, int min, int max)
        {
            Target = target;
            MinRange = min;
            MaxRange = max;
        }

        public Object Target { get; set; }
        public int MinRange { get; set; }
        public int MaxRange { get; set; }
    }
}
