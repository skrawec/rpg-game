﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public static class WordGenerator
    {
        public static string PossissiveGenderWord(Gender geneder)
        {
            if (geneder == Gender.Female)
            {
                return "her";
            }
            else if (geneder == Gender.Male)
            {
                return "his";
            }
            else
            {
                return "its";
            }
        }

    }
}
