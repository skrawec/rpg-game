﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class CombatHelper
    {        
        public static decimal CalcDamage(Random random, Object objectUsed, Creature weidler)
        {            
            int minDamage = (int)Math.Abs((objectUsed.Weight / 500) - 1);
            int maxDamage = (int)Math.Abs((objectUsed.Weight / 500) + 1);


            var damage = random.Next(minDamage == 0 ? 1 : minDamage, maxDamage);

            decimal baseDamage = objectUsed.Material.Hardness * damage;

            decimal strengthModifier = (decimal)weidler.Strength / 10M;

            decimal totalDamage = baseDamage * strengthModifier;

            return totalDamage;
        }

        public static decimal CalcDamage(Random random, Weapon weaponUsed, Creature weidler)
        {
            int minDamage = (int)Math.Abs((weaponUsed.Weight / 500) - 1);
            int maxDamage = (int)Math.Abs((weaponUsed.Weight / 500) + 1);


            var damage = random.Next(minDamage == 0 ? 1 : minDamage, maxDamage);

            decimal baseDamage = weaponUsed.WeaponPiece.Material.Hardness * damage;

            decimal strengthModifier = weidler.Strength / 10M;

            decimal totalDamage = baseDamage * strengthModifier;

            return totalDamage;
        }

        public static decimal CalcResist(Weapon weaponUsed)
        {
            return weaponUsed.WeaponPiece.Material.Hardness;
        }

        public static decimal CalcResist(Object objectUsed)
        {
            return objectUsed.Material.Hardness;
        }
    }
}
