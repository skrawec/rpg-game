﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPG.Systems
{
    public enum AttackType
    {
        Blunt,
        Piercing,
        Slashing,
        Fire,
        Ice,
        Bleeding,
        Poison,
        SevereBleeding,
    }
}
