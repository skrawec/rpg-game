﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Systems
{
    public class GenerateMaterial
    {
        private static Material _steel;
        public static Material Steel
        {
            get
            {
                if (_steel == null)
                {
                    Material material = new Material("Steel");
                    material.Density = 7.75M;
                    material.Hardness = 5;
                    material.Worth = 0.55M;
                    material.DestroyedWords.Add("buckled");
                    material.DestroyedWords.Add("destroyed");
                    material.DestroyedWords.Add("shattered");
                    material.IsOrganic = false;

                    _steel = material;
                }

                return _steel;
            }
        }
        private static Material _bone;
        public static Material Bone
        {
            get
            {
                if (_bone == null)
                {
                    Material material = new Material("Bone");
                    material.Density = 1.9M; //1.9 for bone and 0.35 for skin
                    material.Hardness = 4M; //4.5 Bone and 0.5 for skin
                    material.DestroyedWords.Add("splintered");
                    material.DestroyedWords.Add("crushed");
                    material.DestroyedWords.Add("broken");
                    material.Worth = 0.005M;
                    material.IsOrganic = false;

                    _bone = material;
                }

                return _bone;
            }
        }

        private static Material _skin;
        public static Material Skin
        {
            get
            {
                if (_skin == null)
                {
                    Material material = new Material("Skin");
                    material.Density = 0.8M; //1.9 for bone and 0.35 for skin
                    material.Hardness = 2M; //4.5 Bone and 0.5 for skin
                    material.DestroyedWords.Add("severed");
                    material.Worth = 0.0005M;
                    material.IsOrganic = true;

                    _skin = material;
                }

                return _skin;
            }            
        }
        private static Material _hide;
        public static Material Hide
        {
            get
            {
                if (_hide == null)
                {
                    Material material = new Material("Hide");
                    material.Density = 0.8M; //1.9 for bone and 0.35 for skin
                    material.Hardness = 2.5M; //4.5 Bone and 0.5 for skin
                    material.DestroyedWords.Add("severed");
                    material.Worth = 0.005M;
                    material.IsOrganic = true;

                    _hide = material;
                }

                return _skin;
            }
        }

        private static Material _maple;
        public static Material Maple
        {
            get
            {
                if (_maple == null)
                {
                    Material material = new Material("Maple Wood");
                    material.Density = 0.53M;
                    material.Hardness = 3;
                    material.Worth = 0.14M;
                    material.DestroyedWords.Add("split in two pieces");
                    material.DestroyedWords.Add("splintered");
                    material.DestroyedWords.Add("shattered");
                    material.DestroyedWords.Add("destroyed");
                    material.IsOrganic = false;

                    _maple = material;
                }

                return _maple;
            }
        }
    }
}
